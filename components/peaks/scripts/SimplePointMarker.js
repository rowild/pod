import { Label, Tag } from 'konva/lib/shapes/Label';
import { Line } from 'konva/lib/shapes/Line';
import { Text } from 'konva/lib/shapes/Text';

/**
 * Simple Point marker
 * to be used with peaks' "overview" view
 */
class SimplePointMarker {
  constructor(options) {
    // console.log('SimplePointMarker constructor invoked: options =', options);
    this._options = options;
    // this._colors = ['empty', '#3F83F8', '#0E9F6E', '#FF5A1F']
    // this._pointerHeights = ['empty', 10, 14, 18]
    // console.log('CustomPointMarker invoked, this._options =', this._options);
  }

  init(group) {
    // console.log('SimplePointMarker invoked:')
    // console.log('   group = ', group);
    this._group = group;

    // Vertical Line - create with default y and points, the real values
    // are set in fitToView().
    this._line = new Line({
      x: 0,
      y: 0,
      stroke: this._options.color,
      strokeWidth: 1
    });

    group.add(this._line);

    this.fitToView();
  }

  fitToView() {
    const height = this._options.layer.getHeight();

    this._line.points([0.5, 0, 0.5, height]);
  }
}

export default SimplePointMarker;