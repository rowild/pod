import CustomPointMarker from '../scripts/CustomPointMarker';
import SimplePointMarker from './SimplePointMarker';
// import CustomSegmentMarker from './CustomSegmentMarker';

export function createPointMarker(options) {
  console.log(' ');
  console.log(' ');
  console.log('createPointMarker options =', options);
  console.log(' ');
  console.log(' ');

  if (options.view === 'zoomview') {
    return new CustomPointMarker(options);
  }
  else {
    return new SimplePointMarker(options);
  }
}

// export function createSegmentMarker(options) {
//   if (options.view === 'zoomview') {
//     return new CustomSegmentMarker(options);
//   }
//   return null;
// }