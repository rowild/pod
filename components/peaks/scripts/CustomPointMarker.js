import { Label, Tag } from 'konva/lib/shapes/Label';
import { Line } from 'konva/lib/shapes/Line';
import { Text } from 'konva/lib/shapes/Text';
import { Circle } from 'konva/lib/shapes/Circle';
import { Rect } from 'konva/lib/shapes/Rect';

export default class CustomPointMarker {
  constructor(options, experimentStep) {
    this._options = options;
    this.experimentStep = experimentStep

    // console.log(' ');
    // console.log('CustomPointMarker Class:');
    // console.log('   this.experimentStep =', this.experimentStep);
    // console.log('   this._options =', this._options);

    this.pointHeights = [null, 20, 14, 10]
    this.pointColors = [null, 'rgba(185,28,28,1.0)', 'rgba(29,78,216,1.0)', 'rgba(21, 128, 61, 1.0)'] // red/700, blue/700, // emerald/700: rgba(4,120,87,1.0),  // green-700: rgba(21, 128, 61, 1.0)

    this.pointerW = 12
    this.pointerH = this._options.point.pointerHeight
    this.pointerPadding = 3
    this.color = this._options.point.color;
    this.labelText = this._options.point.labelText
    this.fontSize = 10
  }

  init(group) {
    this._group = group;
    this._label = new Label({ x: 0, y: 0 });
    this._icon_invalid = new Label({ x: 0, y: 0 });
    this._icon_valid = new Label({ x: 0, y: 0 });

    if (this._options.point.deleted) {
      // console.log('   deleted: this._options.point.deleted =', this._options.point.deleted);
      this.color = 'rgba(80,80,80,.15)'

      this.pointerW = 9
      this.pointerH = 7
      this.pointerPadding = 1
      this.labelText = 'x'
      this.fontSize = 8

      this._options.draggable = false
    }
    else {
      // console.log('   point NOT deleted');
    }

    this._tag = new Tag({
      fill: this.color,
      stroke: this.color,
      strokeWidth: 1,
      pointerDirection: 'down',
      pointerWidth: this.pointerW,
      pointerHeight: this.pointerH,
      lineJoin: 'round',
      x: 0,
      y: 0
    });
    this._label.add(this._tag);

    this._text = new Text({
      text: this.labelText,
      fontFamily: 'Verdana',
      fontSize: 10,
      padding: this.pointerPadding,
      fill: 'white'
    });
    this._label.add(this._text);

    this._line = new Line({
      x: 0,
      y: 0,
      stroke: this.color,
      strokeWidth: 1
    });

    this._circle = new Circle({
      radius: 8,
      fill: 'white',
      stroke: this.color,
      strokeWidth: 1,
      x: 0,
      y: -6
    });

    this._rect = new Rect({
      width: 16,
      height: 20,
      fill: 'white',
      stroke: this.color,
      strokeWidth: 1,
      cornerRadius: 2,
      x: -8,
      y: -16,
    })

    this._frame = new Rect({
      width: 24,
      height: 200,
      fill: 'white',
      stroke: 'crimson',
      strokeWidth: 2,
      cornerRadius: 4,
      x: -12,
      y: -20,
    })

    this._questionMark = new Text({
      text: '?',
      fontFamily: 'Verdana',
      fontStyle: 'bold',
      fontSize: 16,
      padding: this.pointerPadding,
      fill: '#ff9900',
      x: -8,
      y: -16
    })

    this._check = new Line({
      // [x1, y1, x2, y2, x3, y3]
      points: [-4, -4, 0, 0, 10, -12],
      stroke: this.color,
      strokeWidth: 3,
      lineCap: 'round',
      lineJoin: 'round',
      x: -1,
      y: -3
    })


    // Add Stuff

    // Exclude offsetMarkers from additional graphics
    // if (
    //   this._options.point._id !== 'peaks.point.1'
    //   && this._options.point._id !== 'peaks.point.9999'
    //   && this._options.point.activated
    //   && this.experimentStep === 2
    // ) {
    //   group.add(this._frame)
    // }

    this._icon_invalid.add(this._rect);
    this._icon_invalid.add(this._questionMark);

    this._icon_valid.add(this._circle);
    this._icon_valid.add(this._check);

    // Order matters!
    group.add(this._line);
    group.add(this._label);

    // Exclude offsetMarkers from additional graphics
    if (
      this._options.point._id !== 'peaks.point.1'
      && this._options.point._id !== 'peaks.point.9999'
      && !this._options.point.deleted
      && this.experimentStep === 2
    ) {
      if (this._options.point.markerDegree < 3 && !this._options.point.validated) {
        group.remove(this._icon_valid)
        group.add(this._icon_invalid)
      } else if (this._options.point.validated) {
        group.remove(this._icon_invalid)
        group.add(this._icon_valid)
      }
    }

    /// Test
    // group.add(this._icon_invalid)
    // group.add(this._icon_valid)

    this.fitToView();
  }

  fitToView() {
    const height = this._options.layer.getHeight();
    const labelHeight = this._text.height() + (2 * this._text.padding() + this.pointerH);

    // leave space for the time inidicators (little vertical lines)
    const offsetTop = 14;
    const offsetBottom = 24;
    const lineHeight = height - offsetTop - offsetBottom;

    this._group.y(offsetTop);
    this._line.points([0.5, 0, 0.5, lineHeight]);
    this._label.y(this.pointerH + offsetTop)
    this._icon_invalid.y(lineHeight - 14)
    this._icon_valid.y(lineHeight - 14)
  }
}

