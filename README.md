# pod-nuxt

A nuxt application using Peaks.js for the "Points of Discontinuity" project by Christian Utz.

## About this app

This app supports a FWF supported science project by Christian Utz. It is powered
by Peaks.js, an audio waveform interface developed by Chris Needham at BBC. He
also provided extensive support and script additions!

This project is dependent on a strapi backend, which is released on heroku :
https://strapi-pod.herokuapp.com/

Assets are uploaded to AWS:
(provide some details)

The workings of this app are mainly streamlined around the fetching of data from
the strapi backend and its saving to vuex and indexeddb (via the dexie.js library).
In the midst of it there is Peaks.js providing a 2-step app along which users
have to analyse formal aspects of music of the 20th & 21st century.

## Build Setup

Warning! `npm` has a lot of problems with dependencies (webpack 5, sass-loader, ...)
See: https://github.com/vuejs/vue-cli/issues/5986
`yarn` doesn't.

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

# create change log and increase version number
$ yarn release
```

### Notes & ToDos

- OK - Marker Degrees (nicht level) => Offset marker sind besonders wichtig, also Marker Degree 3 – trotzdem, weil nicht user-editierbar, als Marker Degree 0 in zB gelb
- OK - Offset Marker von Anfang an sichtbar
- OK - Toggles übers Backend steuerbar
- OK - "Tasks" statt "Stage" // actually: Step(s)
- Degree Farben anpassen

## How to optimize the app

The current state of the app is quite chaotic. Here are some hints what should
be done do improve that situation:

- Reduce numbers of modals
  - All button info modals should be only 1 modal that receives a params object
- Strapi: Clean up the modals for button info fields. That also means naming them correctly from the beginning, because overriding them does not change the underlyind DB structure

# TODO on Dec 24the ff

- pilot: save to indexeddb process must be fixed (so that, when coming back to the audio list, the analysed files are checked off)
- pilt: save between steps (moving cues) must be checked
- pilot: save to strapi (as a "result") must be checked: needs participant id, birthday...

# NEW MODAL CONCEPT

INFO MODAL

- no buttons (except "close")
- for, e.g., simple information about buttons in playground
- simple strapi backend
- closeable via click-outside, ESC
- has 2 params: `modal_domain` and `content_domain`

```js
@click="$nuxt.$emit('showInfoModal', {
  modal_domain: 'INFORMATION_ABOUT_BUTTONS',
  content_domain: 'TASK_INSTRUCTION_BUTTON'
})"
```

CONTENT MODAL

- no buttons (except "close")
- strapi: content with repeatables
- e.g.: TASK_ONE, GDPR, IMPRINT...
- stricter, when it comes to closing (only close button)
- has 1 param only: `domain`

```js
@click="$nuxt.$emit('showContentModal', {
  domain: 'TASK_TWO'
})"
```

FEEDBACK MODAL

- has buttons (to be configured via a json field in strapi)
  `btn_type` will be resolved to `btn-${btn.btn_type}` that must exist in css

```json
{
  "introduction": {
    "btn_txt": "To Introductions",
    "btn_type": "success",
    "route": "/introduction"
  },
  "playground": {
    "btn_txt": "To Playground",
    "btn_type": "success",
    "route": "/playground"
  },
  "experiment": {
    "btn_txt": "To Experiment",
    "btn_type": "success",
    "route": "/experiment"
  },
  "pilot": {
    "btn_txt": "To Pilot Experiment",
    "btn_type": "success",
    "route": "/pilot"
  }
}
```

- strapi: content with repeatables (e.g. for TASK_ONE, GDPR, IMPRINT...)
- strict mode (defines closing options: ESC, click-outside, close button)
- has 6 params:

```js
@click="$nuxt.$emit('showFeedbackModal', {
  message: 'You participant ID: ' + query[0].id,
  message_type: 'success',
  modal_domain: 'REGISTRATION',
  content_domain: 'ALREADY_REGISTERED',
  strict: false,
  phase: this.getSettings.phase
})"
```

Special modals remain, e.g:
- ExperimentStepOneWithPreloader

# TODO for Jan 1st 2022

- DONE: add descriptors and its methods
- DONE: add new Konva graphics for "info missing" / "info given" on marker 2 and 1
- DONE: add textfield for additional remarks

# TODO for Jan 2nd & 3rd 2022

- DONE: implement all VueX save methods concerning the descriptor features (currently VueX is not updated)
- DONE: save point to VueX after drag
- DONE: implement correct counter for "max. available markers"
- DONE: activate SAVE button, when all non-deleted markers are validated
- DONE: fix clicktrack controls visibility (in step 2, volume did not visually adjust)
- DONE: remove layout jump when showing "Spacebar cannot be used..." warning
- DONE: save to DB after experiment is finished
- DONE: add "isTesting" var to circumvent certain things in order to speed up development
- DONE: delete key event listeners (otherwise markers will be added after 1st task has been finished and the selection list is displayed again)

# TODO for Jan 4 2022

- DONE: Implement an online check, whether user still exists (user can be deleted after a grace period)
- DONE: Strapi: provide "grace period" and "grace_period_warning" fields (days a subscription is valid; days within the epxeriment must be completed)
- DONE: Nuxt: when registered, show message until when the experiment must be finished
- DONE: On experiment save, make sure there is a flag that sets the experiment (and user?) to "done" (in strapi, project), when all tasks are finished

# TODO for Jan 6+ 2022

- DONE: Pilot needs a "final note" text field (and strapi field)
- DON: When saving to strapi, "gender" must be added to "participant_data"
- ? When saving to strapi, "descriptor name" (as text) should be added to each marker

# TODO for Jan 10 2022

- DONE: Waveform Header: Put tiles next to each other, so overview waveform has more space on screen with large font
- DONE: fix saving of font size to indexedDB
- DONE: Hide Metronome icon in Sstep 1; also: fix voume number displa; make drag animation quicker
- DONE: Hide navigation only, when experiment begins
- DONE: Make descriptor window larger
- DONE: Font resizer should be active, when isExperiment is active and ongoing

# TODO for Jan 10 2022

- DONE: Experiments must be done in specific order
- DONE: Fix Descriptor bug: selecting descriptors - closing - opening again - selecting new descriptors -closing - opening again: initial descriptors were not remembered

# TODO for Jan 26 2022

- DONE: Make close button in modals (GDPR, Terms & Conditions) visible all the time (quasi fixed)

# TODO Audosave implementation

There are already some save guards implmented:
- beforeRouterLeave
- window.addEventListener('beforeunload', function (e) {

In order to minimize data loss on unexpected browser or user behaviour, implement a timer that saves a snapshot of the
current vuex experiment store to indexedDB (esp. cues and audio ID). The app must then check the indexedDB table
"snapshot", if there is data available (snapshot[0].cues.length > 2 [2 is the nuber of offset cues, which are always
available!])

It needs to places in app to check, whether there is data available:
- on startup
- on each route

What could happen?
- both "snapshot detectors" could be initiated: page on first load & route

So: move the snapshot detection to the router?

Q: Could this feture be simply replaced by using "keep-alive"?
A: No! Way to many aspects - like is the task already saved? - have to be (re)implemented

# TODO for Mar 8 2022

PLAYGROUND:
Continue restoring snapshot in Wavefrom.vue and peaks.vue

- OK – OffsetMarkers are doubled on restore - find a way to add them only, when they
    do not already exist
      or de-dupe the array?

      // https://dev.to/soyleninjs/3-ways-to-remove-duplicates-in-an-array-in-javascript-259o
      chars.reduce((acc, char) => acc.includes(char) ? acc : [...acc, char], []);

      let chars = ['A', 'B', 'A', 'C', 'B'];
      let uniqueChars = [...new Set(chars)];
      console.log(uniqueChars);

    Solution: the setup was divided in a if(restore=true) {} else {} logic;
        create (new) offset markers in else-block only

- OK – start playback from last cue automatically, when step 1 is restored (good for long files)
- OK – display a "restored" text somewhere
- OK - save and restore audio track volume and click track volume, too
- OK – settings new markers in restored version does not work (this.offsetStartTime must be created when coming from restoreTask)

This all has also to be done for playground and experiment!!!

PILOT
- make sure pilot audio list is always loaded from server (strapi), not dexie

- EXPERIMENT

# TODO for Mar 16 2022

- check snapshot feature with pilot
- OK – implement contextmenu
- implement save to finished_tasks table in vuex experiment store
- implement review

SNAPSHOT needs more info

- phase / better: route? (playground, pilot, experiment)
- invite code
- pilot / experiment participant ID  ?
- pilot / experiment ID  ?

When phase (=route) found - redirect!
When invitecode found, check with DB: if found keep going, if not, redirect to enter invite code

Find a way to get tasks by filename!
"filename" must be unique!


# TODO for Mar 27 2022

- OK - improve waveform layout (get rid of some bad paddings and the w-0 problem of
  the amplitude scale buttons on the right, which only show in experiment step 2)
- OK - improve zoom behavior: "fit()" is independent of the input scale next to it; in
  fact: fit() mutes the input, when active; when deactivated, waveform scale jumps
  to previous scaling
- OK - implement canvas resize (reacts to font resize and window-resize)
- Right-click menu causes troubles on small font sizes and resolutions on Windows
  It seems this is caused by a click detection on the popup menu itself!
  how about to wither change the translateY distance or to check the popupmenu
  with JS and suppress context menu on the popupmenu?


--------------------------------------------------------------

# STRUCTURE OF THE BACKUP FUNCTION

In Playground a backup is detected; a modal provides info and choices.

In Peaks.js the backup function is "switched on" and "switched off".

In Waveform.vue, the restoreTask function is invoked via a prop.


--------------------------------------------------------------

# REMEMBER

The following steps where temporarily disabled for developing purposes. They must
be re-enabled again.

- activate validation in registration form again
- activate experimentStep=1 & first modal again in Waveform
- activate opening  Modal in experimentStep 2 again

Introduced a new vuex var for that: isTesting

### UNDO things that were used for developing / testing aspects

ok - next step button in waveform (remove exlamation mark from is Development)
ok - Reset button in pilot (ditto)

# Peaks.js Problems

- Zoomview.fitToContainer() does not work, when parent of canvas is styled with flex?
- Scaling fom "auto" to another level is not so cool



// src: https://stackoverflow.com/questions/5072136/javascript-filter-for-objects/37616104
// Object.filter = (obj, predicate) => Object.fromEntries(Object.entries(obj).filter(predicate));