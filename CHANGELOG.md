# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.30](https://bitbucket.org/rowild/pod/compare/v0.0.29...v0.0.30) (2022-04-17)


### Features

* hide plahead is seekable switch ([6bc6edd](https://bitbucket.org/rowild/pod/commit/6bc6edd1daa7965f7721246512fd85ff221d9f4c))
* implement dat and json ([a45f329](https://bitbucket.org/rowild/pod/commit/a45f329a371e03b5ad1bed137238dd3352abfba8))
* show info about the composition just heard in modal and final table ([e3b2b35](https://bitbucket.org/rowild/pod/commit/e3b2b356a019075da17016385db34252c5289c19))
* use DAT file ([f551ada](https://bitbucket.org/rowild/pod/commit/f551ada82fb41d2078ea091d546f3d3e1d50fc68))


### Bug Fixes

* show modal in step 1 of restore snapshot again to show loading process ([61a0790](https://bitbucket.org/rowild/pod/commit/61a07904e612215db3bf627eb4cf67d51a03fb4f))

### [0.0.29](https://bitbucket.org/rowild/pod/compare/v0.0.28...v0.0.29) (2022-03-29)


### Features

* add reset registration button to secret button in footer left ([486c45b](https://bitbucket.org/rowild/pod/commit/486c45b73b09547821455c6ea242d79375661870))
* improve registration and snapshot detection procedure... ([dd6e21e](https://bitbucket.org/rowild/pod/commit/dd6e21e9c8c0b16e1ce058cee475c120cc00703e))


### Bug Fixes

* unset restoreTask properly and in the right places ([bc5f89f](https://bitbucket.org/rowild/pod/commit/bc5f89f54aa9df70a370b9d78c095255583050de))
* use spread operator on cues and cuesOfStepOne in store/experiment to properly restore the snapshot ([3168d10](https://bitbucket.org/rowild/pod/commit/3168d10a68b7a7c30f690d9930141acb5ee9a989))

### [0.0.28](https://bitbucket.org/rowild/pod/compare/v0.0.27...v0.0.28) (2022-03-28)


### Features

* better layout for exit page ([6512f6e](https://bitbucket.org/rowild/pod/commit/6512f6e74df33855f658d9dbb36b1818ec3f90ca))
* prevent contextmenu on rightclick menu popup ([a9a92d3](https://bitbucket.org/rowild/pod/commit/a9a92d33b07a192d9519115ea94dd44bddac2f55))


### Bug Fixes

* clone array AND clone its objects by using spread operator ([8d36065](https://bitbucket.org/rowild/pod/commit/8d36065925395a4ed115e66f21cd1df2c69b296a))

### [0.0.27](https://bitbucket.org/rowild/pod/compare/v0.0.26...v0.0.27) (2022-03-27)


### Features

* improvements ([3855ead](https://bitbucket.org/rowild/pod/commit/3855eadd24ca04519d1d71b00e30ac7207cba604))

### [0.0.26](https://bitbucket.org/rowild/pod/compare/v0.0.25...v0.0.26) (2022-03-26)


### Features

* adapt contextmenu - grey-out non-avilable options ([cc8e6e9](https://bitbucket.org/rowild/pod/commit/cc8e6e9451512a9693d8b289fc20ac7652a34e17))
* add a css pixel-device-ratio query to be displayed in the footer ([389127e](https://bitbucket.org/rowild/pod/commit/389127e7c27f295e5873fcd23f8e72b14c2a01c6))
* add another custom marker example ([036cb6e](https://bitbucket.org/rowild/pod/commit/036cb6e436ddbb7055cdabf746243e98b9d0d755))
* add stopProgagation and stopImmediatePropagation to rightclick menu ([1dfbe95](https://bitbucket.org/rowild/pod/commit/1dfbe95540b3a29a63ded89bd199bd403cb7e27c))
* autosave (WIP) ([c16b30d](https://bitbucket.org/rowild/pod/commit/c16b30d0bac370840c016005643d6a542ef74c55))
* autosave (WIP) ([932857a](https://bitbucket.org/rowild/pod/commit/932857a0dfbfb3af64bede1bbf7c82660fb1c98f))
* autosave (WIP) ([c1fdac6](https://bitbucket.org/rowild/pod/commit/c1fdac66ab2adf52376c0761caf8896de56a3265))
* autosave (WIP) ... ([10ee1ee](https://bitbucket.org/rowild/pod/commit/10ee1ee704061f64c9f6a43a9f9c32ad7a004e1e))
* autosave (WIP) ... ([641d9ff](https://bitbucket.org/rowild/pod/commit/641d9ff4f32e787f1a70599617c19501cfef3fda))
* autosave (WIP) ... ([2589e58](https://bitbucket.org/rowild/pod/commit/2589e5862b21667fc26c59c83a9f2aae1eeb9324))
* autosave (WIP) ... ([974fa09](https://bitbucket.org/rowild/pod/commit/974fa09d46b21d981312bd04e03e246e1b9f2b46))
* autosave (WIP) ... ([d0452c1](https://bitbucket.org/rowild/pod/commit/d0452c1a9c944735343417a114f8ac26a4cf64f3))
* autosave (WIP) ... ([98676ba](https://bitbucket.org/rowild/pod/commit/98676ba134d8749e929d7890274e2acfb57a002c))
* contextmenu, peaks 1.0, zooming, new dexie ([a766a6f](https://bitbucket.org/rowild/pod/commit/a766a6f70042b2e6f9817306efc518b233d1c6dd))
* extending snapshot feature ([97be646](https://bitbucket.org/rowild/pod/commit/97be64639ca9b30e72aba243c4d1d00e2f057498))
* integrate snapshot detection and handling in pilot ([008346c](https://bitbucket.org/rowild/pod/commit/008346c957e73e281bd8923e2e29be7bf9c55bf3))
* integrate snapshot detection and handling in pilot, part 2 ([3663f8a](https://bitbucket.org/rowild/pod/commit/3663f8a450369adda6757f4ec90154ae6629c215))
* remove paddin from peaks canvas ([3fedf31](https://bitbucket.org/rowild/pod/commit/3fedf314e67865c492dbb59884c5acbe04595857))
* set all possible cache controls to 0 an no-cache in order to always fetch the scripts from the server ([197a512](https://bitbucket.org/rowild/pod/commit/197a512d71edaaacf6b1834d8acee54ffee9fdd7))


### Bug Fixes

* move .toLowerCase() to a later spot ([930b2e7](https://bitbucket.org/rowild/pod/commit/930b2e76580ebd30b5a3034c3d17ca5ad57ef02f))
* move v-if to li tag in menu ([a6da2cb](https://bitbucket.org/rowild/pod/commit/a6da2cb5c3cfd04bffd5397f5235d569b0919f35))
* remove reading from URL ([4a8d483](https://bitbucket.org/rowild/pod/commit/4a8d483f3ad9913fe38ba18b9cbcf0164f968fde))
* remove text "CONTENT" from ContentModal ([3db00e8](https://bitbucket.org/rowild/pod/commit/3db00e8147f21b109d9b58563f17848236113ac3))
* remove text "CONTENT" from ContentModal via checkout from master branch ([d23e4c8](https://bitbucket.org/rowild/pod/commit/d23e4c852fd039fca0ef2b303470bbb79423f5e6))

### [0.0.25](https://bitbucket.org/rowild/pod/compare/v0.0.24...v0.0.25) (2022-03-03)


### Features

* add more buttons to have things like space-between available ([82ac935](https://bitbucket.org/rowild/pod/commit/82ac935da4004bb4d5b5b06ad51a89af3a030a70))
* add more text to warning ([65e277e](https://bitbucket.org/rowild/pod/commit/65e277e35f0114850bf4ac4a42f09e741a865e43))
* configure nuxt with environment variables; prepare Vercel with env var settings, too ([1f6a697](https://bitbucket.org/rowild/pod/commit/1f6a6977074f5bf8349dc97e3b17926ca673db51))
* implement duration field that shows how long a piece lasts ([64a9fee](https://bitbucket.org/rowild/pod/commit/64a9fee2cb114a4c9825b0b86a1bec45384f1624))
* leave-route warning via Vue`s router implemented hoping this catches the swipe action on MacBooks ([4cc0996](https://bitbucket.org/rowild/pod/commit/4cc09966a5dc429eaee0206aa6f6bca408020052))
* new page environment (shows env vars, needed because of potential clone for pod presentation) ([fca41e2](https://bitbucket.org/rowild/pod/commit/fca41e2256b7fc4d67513962a3ae7a974736f06a))


### Bug Fixes

* get the wipe action under control using overscroll-behavioue:noe ... (test 1) ([a4a6903](https://bitbucket.org/rowild/pod/commit/a4a6903c0bbde87306a62018a603071c7b5d2cba))
* make width of task buttons equal ([e98b751](https://bitbucket.org/rowild/pod/commit/e98b751052c251760f08a73553f6182b923a725d))
* wrong function name for console.group() ([6dcec33](https://bitbucket.org/rowild/pod/commit/6dcec334e093709d4b128a1e6e8b544c2d7bc9b5))

### [0.0.24](https://bitbucket.org/rowild/pod/compare/v0.0.23...v0.0.24) (2022-02-27)


### Features

* adapt feedback modal for potential error message on no-audio-found ([2e38976](https://bitbucket.org/rowild/pod/commit/2e389764b448372cb74148e0123c796bbe062f7e))
* add descriptor visibility ([c507f7a](https://bitbucket.org/rowild/pod/commit/c507f7af98ed911f260e37749ce2cf10c8a0d665))
* integrate check for missing audio and its error message; fix refresh method ([d422232](https://bitbucket.org/rowild/pod/commit/d422232f00885652f5d9ada3ca78bc3c10c56ad7))


### Bug Fixes

* grammar - change "Salutation" to "Title" in registration form ([9456ff9](https://bitbucket.org/rowild/pod/commit/9456ff9d29b569320403fdd8af91b9589c16a13c))

### [0.0.23](https://bitbucket.org/rowild/pod/compare/v0.0.22...v0.0.23) (2022-02-15)


### Features

* maintenance mode integration ([223a882](https://bitbucket.org/rowild/pod/commit/223a882955406474df89e3f35b994915319d5182))
* show descriptor index numbers only in development mode ([76fbf4e](https://bitbucket.org/rowild/pod/commit/76fbf4e069c25cb126adda2bb4331ecf8da47ea0))

### [0.0.22](https://bitbucket.org/rowild/pod/compare/v0.0.21...v0.0.22) (2022-02-12)


### Bug Fixes

* hide list of files, when all are finished, to show finished button ([5ea6db8](https://bitbucket.org/rowild/pod/commit/5ea6db8edeaa964b58ee12a6bf7853ecde48acc2))

### [0.0.21](https://bitbucket.org/rowild/pod/compare/v0.0.20...v0.0.21) (2022-02-07)


### Bug Fixes

* video size ([57bc47f](https://bitbucket.org/rowild/pod/commit/57bc47f9719b5f2c5093c3bd28f10a114e868054))

### [0.0.20](https://bitbucket.org/rowild/pod/compare/v0.0.19...v0.0.20) (2022-01-30)


### Bug Fixes

* make clickSound a promise, too, and detect onend properly ([2f0d6ed](https://bitbucket.org/rowild/pod/commit/2f0d6ed249fd9fe04edbc359720e8d60dd64de9a))
* move "Delete marker" over "Remark" ([3187d71](https://bitbucket.org/rowild/pod/commit/3187d71c29c6f702ac7bb1b9f3fe32b30f1fa509))

### [0.0.19](https://bitbucket.org/rowild/pod/compare/v0.0.18...v0.0.19) (2022-01-26)


### Features

* modal´s close button is always visible ([f343f64](https://bitbucket.org/rowild/pod/commit/f343f64a34c10521d46f383248df55beb01b3b47))


### Bug Fixes

* improve modal layouts ([9e71f17](https://bitbucket.org/rowild/pod/commit/9e71f17f574b22bb62b80a1e19c9fbfc1e9b2bac))

### [0.0.18](https://bitbucket.org/rowild/pod/compare/v0.0.17...v0.0.18) (2022-01-25)


### Features

* add descriptor list global modal ([f247e96](https://bitbucket.org/rowild/pod/commit/f247e96f6459f8f518129efbb6d144d829069575))

### [0.0.17](https://bitbucket.org/rowild/pod/compare/v0.0.16...v0.0.17) (2022-01-25)


### Features

* add active menu item when in peaks route ([2db915f](https://bitbucket.org/rowild/pod/commit/2db915f963bb95ca1c40f38c72db876406e0048c))
* hide all refresh functions and dev infos and put a mousedown counter on it to make it visible ([03ffcfa](https://bitbucket.org/rowild/pod/commit/03ffcfaa2968b76e31b5a5d782ab2beede1a0156))

### [0.0.16](https://bitbucket.org/rowild/pod/compare/v0.0.15...v0.0.16) (2022-01-25)


### Bug Fixes

* change text in exit ([eea5606](https://bitbucket.org/rowild/pod/commit/eea5606e1243a58bdebdd343d60bac72c785fbc3))

### [0.0.15](https://bitbucket.org/rowild/pod/compare/v0.0.14...v0.0.15) (2022-01-13)


### Bug Fixes

* do not save when too many markers ([29d2bd0](https://bitbucket.org/rowild/pod/commit/29d2bd051d08a46ad5513e961e763a7631a60e29))
* fitToContainer() needs padding-right ([efab35a](https://bitbucket.org/rowild/pod/commit/efab35a9a8e111d697e4f4660d749474238453a0))
* offsetEndMarker has id 9999, not 2 ([68d1f33](https://bitbucket.org/rowild/pod/commit/68d1f33c5f246d54256d7a8864113ca5bf15f1b0))
* rename function; remove peaks settings ([6099707](https://bitbucket.org/rowild/pod/commit/60997078522a8e3b37fb7b8d4b9797cc5afceb2a))

### [0.0.14](https://bitbucket.org/rowild/pod/compare/v0.0.13...v0.0.14) (2022-01-12)


### Features

* force sequence of task files; easter eggs ([73ccc28](https://bitbucket.org/rowild/pod/commit/73ccc289e0fb848c639d68cc9ef42a0b90456f77))


### Bug Fixes

* show correct markers on er-opening descriptor modal ([7240a01](https://bitbucket.org/rowild/pod/commit/7240a01bca87b8279d4e97c2585dc7cfcaffac8f))

### [0.0.13](https://bitbucket.org/rowild/pod/compare/v0.0.11...v0.0.13) (2022-01-10)


### Features

* add experiment_finished put query and delete indexeddb when all tasks complete ([177711c](https://bitbucket.org/rowild/pod/commit/177711c90dded5ddc20bebc20c1b31ec05128fcd))
* add feedback message box to final pilot step ([8865e93](https://bitbucket.org/rowild/pod/commit/8865e93d607e55226c24c57beccb8c4099d022da))
* implement a grace period message ([0422a48](https://bitbucket.org/rowild/pod/commit/0422a486a9826f1015c8975cbb3910025a4f3fcf))
* implement an online registration check verification ([3af1e2c](https://bitbucket.org/rowild/pod/commit/3af1e2c6ef1fd61868af6ba692ffe91d62128d28))


### Bug Fixes

* add more checks against playground situation ([7111f9d](https://bitbucket.org/rowild/pod/commit/7111f9d9e49340644b0d6d3e6b82d283072579f7))
* another fix due to regData ([c5b36c8](https://bitbucket.org/rowild/pod/commit/c5b36c8c0e6d3a98f31b3b026a1e1a94c73d8a97))
* content modal has wrong font; refresh analyzed files has no db exists check ([fbe3fc3](https://bitbucket.org/rowild/pod/commit/fbe3fc3650fd9c02947f202bb0b908242fe0da0a))
* exchange $strapi with $axios ([4bb50e9](https://bitbucket.org/rowild/pod/commit/4bb50e9ce3cffc04506bcd579b6b6c35d21828e1))
* fontSize is not recognised on server ([81de276](https://bitbucket.org/rowild/pod/commit/81de2765ffd0dae015f1cc4606e79eab9833ca78))
* fontSize is not recognised on server, part 2 ([e7e73bb](https://bitbucket.org/rowild/pod/commit/e7e73bb3c341b887a1099fafdf4c1521d832a89b))
* improve feedback during submit process; fix show form validation errors when form is reset ([921f07c](https://bitbucket.org/rowild/pod/commit/921f07c4208008af983c71aa3461cf69e50ea69d))
* invite code not found ([dbac5a9](https://bitbucket.org/rowild/pod/commit/dbac5a9bb2735cb8956b502f3e1b4f9f025c144a))
* invite code not found, part 2... (WIP) ([ffd4eec](https://bitbucket.org/rowild/pod/commit/ffd4eecb5205c4623168a3e3829961c90e5092d7))
* invite code not found, part 3... (WIP) ([e899160](https://bitbucket.org/rowild/pod/commit/e899160448c0bd293c0516d90405429215364b9a))
* invite code not found, part 4... (WIP) ([1ba95d0](https://bitbucket.org/rowild/pod/commit/1ba95d0519a62458aa9142c2ffa84abb1dc98c37))
* layout fixes ([dd17601](https://bitbucket.org/rowild/pod/commit/dd176019b0884c9e02d2071a5d6db1e476254d7b))
* lots of fixes concerning registration ([0b5a85c](https://bitbucket.org/rowild/pod/commit/0b5a85ccf22d545ee9240f5ac1273f28c5a3831a))
* playgroundInfo must be defined as array with the required object parameters ([e8f6a52](https://bitbucket.org/rowild/pod/commit/e8f6a528b335abf0d1da22a28279e9cefc34184b))
* re-add logictoproceedToNext...Step again ([c89b8bf](https://bitbucket.org/rowild/pod/commit/c89b8bf5c74e5d37cd105544b4e71139819e58d5))

### [0.0.12](https://bitbucket.org/rowild/pod/compare/v0.0.11...v0.0.12) (2022-01-07)


### Features

* add experiment_finished put query and delete indexeddb when all tasks complete ([177711c](https://bitbucket.org/rowild/pod/commit/177711c90dded5ddc20bebc20c1b31ec05128fcd))
* add feedback message box to final pilot step ([8865e93](https://bitbucket.org/rowild/pod/commit/8865e93d607e55226c24c57beccb8c4099d022da))
* implement a grace period message ([0422a48](https://bitbucket.org/rowild/pod/commit/0422a486a9826f1015c8975cbb3910025a4f3fcf))
* implement an online registration check verification ([3af1e2c](https://bitbucket.org/rowild/pod/commit/3af1e2c6ef1fd61868af6ba692ffe91d62128d28))


### Bug Fixes

* add more checks against playground situation ([7111f9d](https://bitbucket.org/rowild/pod/commit/7111f9d9e49340644b0d6d3e6b82d283072579f7))
* another fix due to regData ([c5b36c8](https://bitbucket.org/rowild/pod/commit/c5b36c8c0e6d3a98f31b3b026a1e1a94c73d8a97))
* content modal has wrong font; refresh analyzed files has no db exists check ([fbe3fc3](https://bitbucket.org/rowild/pod/commit/fbe3fc3650fd9c02947f202bb0b908242fe0da0a))
* fontSize is not recognised on server ([81de276](https://bitbucket.org/rowild/pod/commit/81de2765ffd0dae015f1cc4606e79eab9833ca78))
* fontSize is not recognised on server, part 2 ([e7e73bb](https://bitbucket.org/rowild/pod/commit/e7e73bb3c341b887a1099fafdf4c1521d832a89b))
* improve feedback during submit process; fix show form validation errors when form is reset ([921f07c](https://bitbucket.org/rowild/pod/commit/921f07c4208008af983c71aa3461cf69e50ea69d))
* invite code not found ([dbac5a9](https://bitbucket.org/rowild/pod/commit/dbac5a9bb2735cb8956b502f3e1b4f9f025c144a))
* invite code not found, part 2... (WIP) ([ffd4eec](https://bitbucket.org/rowild/pod/commit/ffd4eecb5205c4623168a3e3829961c90e5092d7))
* invite code not found, part 3... (WIP) ([e899160](https://bitbucket.org/rowild/pod/commit/e899160448c0bd293c0516d90405429215364b9a))
* invite code not found, part 4... (WIP) ([1ba95d0](https://bitbucket.org/rowild/pod/commit/1ba95d0519a62458aa9142c2ffa84abb1dc98c37))
* lots of fixes concerning registration ([0b5a85c](https://bitbucket.org/rowild/pod/commit/0b5a85ccf22d545ee9240f5ac1273f28c5a3831a))
* playgroundInfo must be defined as array with the required object parameters ([e8f6a52](https://bitbucket.org/rowild/pod/commit/e8f6a528b335abf0d1da22a28279e9cefc34184b))
* re-add logictoproceedToNext...Step again ([c89b8bf](https://bitbucket.org/rowild/pod/commit/c89b8bf5c74e5d37cd105544b4e71139819e58d5))

### [0.0.11](https://bitbucket.org/rowild/pod/compare/v0.0.10...v0.0.11) (2022-01-03)


### Features

* continue implementation of descriptor management ([d801aed](https://bitbucket.org/rowild/pod/commit/d801aed8c08177343465ead1fecb866768fb919d))
* continue management of descriptors ([f90233d](https://bitbucket.org/rowild/pod/commit/f90233d1acffc06b21a5303a1136c0aa7168bdf9))
* implement descriptors (WIP) ([b253c7f](https://bitbucket.org/rowild/pod/commit/b253c7fbb301b717c3b59ae3caed3ff0bdb0b9be))
* implement development as a strapi setting ([ed84417](https://bitbucket.org/rowild/pod/commit/ed844171859e41a5d78dd48535bb1b10d5c1ba6b))
* implement save to strapi ([14b3a2b](https://bitbucket.org/rowild/pod/commit/14b3a2b9de0b3437b4df3bc27d68656363f1313f))
* implement vuex methods with descriptors ([5e7afe9](https://bitbucket.org/rowild/pod/commit/5e7afe9f30a8ac6dfee4664f138a69d9a713a72c))
* management of descriptor selection implemented ([98cd454](https://bitbucket.org/rowild/pod/commit/98cd454dd38d9dee0d33ee27c8654f9ab4e6c667))


### Bug Fixes

* minor fixes with color, clicktrack ([c5702c5](https://bitbucket.org/rowild/pod/commit/c5702c5a2a40122dcbcb053e6454b29095008e48))
* trying to make the vercel verison work again ([d0c5174](https://bitbucket.org/rowild/pod/commit/d0c51746efb5df8954b8e2ca37ae9846f0776520))

### [0.0.10](https://bitbucket.org/rowild/pod/compare/v0.0.9...v0.0.10) (2021-12-30)


### Features

* add a development helper ([4dcb8fc](https://bitbucket.org/rowild/pod/commit/4dcb8fc5ffe54e115e643385535765cfdb9b6a72))
* add descriptors (WIP) ([02201dc](https://bitbucket.org/rowild/pod/commit/02201dc98a9d5901129ca73ab6f02ef3cff0e047))
* contiue implementing the pilot registration ([ce95f41](https://bitbucket.org/rowild/pod/commit/ce95f4101b49a27b87902697284b7dab4269fdff))
* hide logo and more when experiment active ([7f2a0b8](https://bitbucket.org/rowild/pod/commit/7f2a0b899aafeac5212e0b3abe92ffabfa684caa))
* hide menues when experiment is active ([88fe79d](https://bitbucket.org/rowild/pod/commit/88fe79d34a577c4d609cd50e8ebd4e49208e09f8))
* implement over-count of markers ([b7e5250](https://bitbucket.org/rowild/pod/commit/b7e5250163f9312e54daec36635fa9ce57f86f6e))
* integrate the development helper (see a previous commit) ([83afb99](https://bitbucket.org/rowild/pod/commit/83afb997f16b39d1d034c9e380fdbacf9e86bb42))
* save pilot and expertiment results, download results ([2ee8456](https://bitbucket.org/rowild/pod/commit/2ee84563d80b38f60b377b95eab97e828d107cee))
* save routine must distingish between playground, pilot and expert "app section state" ([e8eeea5](https://bitbucket.org/rowild/pod/commit/e8eeea567837bc33dd7ea912b64c9b8348c2296c))
* start implementing pilot experiments ([303d563](https://bitbucket.org/rowild/pod/commit/303d563be53f841d2239722b71fe271ea66e07a1))


### Bug Fixes

* add markers in step 1 only when audio already plays ([7701c85](https://bitbucket.org/rowild/pod/commit/7701c85b0896d83bfb5ff040cee604001c43e39b))
* add markers in step 1 only when audio already plays (addOn to earlier commit) ([f25f78c](https://bitbucket.org/rowild/pod/commit/f25f78c42b8dba0292f103bc02774494fd74c432))
* implement proper way to stop audio playback promise ([5068dc3](https://bitbucket.org/rowild/pod/commit/5068dc311fcaed34a305edfce0de97463a0a3eda))
* remove "in modal" text from svg icon title ([d41868f](https://bitbucket.org/rowild/pod/commit/d41868f06acd26f7c9b2ed491485a0169a367338))

### [0.0.9](https://bitbucket.org/rowild/pod/compare/v0.0.7...v0.0.9) (2021-12-20)


### Features

* add 2 new icons for imprint and gdpr in sidebar ([f385147](https://bitbucket.org/rowild/pod/commit/f3851476c0502b057768ddc28031d1c77e376c8a))
* add more keyevents for scaling the waveform and fitting it. ([8028ad9](https://bitbucket.org/rowild/pod/commit/8028ad97942141070074c010fac792f0cf16936e))
* add not-registered check on experiment page ([0ddbdaa](https://bitbucket.org/rowild/pod/commit/0ddbdaa805f335f5f6851dae32cfd65ba790042d))
* adding symbols to CustomPointMarker (WIP) ([b94478b](https://bitbucket.org/rowild/pod/commit/b94478b465c5ce904feb29b16dcdb2daf93f8c3e))
* Beginning to implement desrciptors ([904533f](https://bitbucket.org/rowild/pod/commit/904533f0f01a81c1f69003b84aeb076f1e3fc130))
* form submission and check implementation, part 1 (WIP) ([4b017da](https://bitbucket.org/rowild/pod/commit/4b017dac2ad7b32e667e7fbf23efc7825f6544df))
* implement imprint and gdpr correctly ([f44427c](https://bitbucket.org/rowild/pod/commit/f44427c7d018705b20b1c5f2d3a10ac5481de688))
* new key event with e.which ([e584017](https://bitbucket.org/rowild/pod/commit/e584017a79485bba30259beebd37e94f2d67dae7))
* new waveform zoom sliders ([6ee8901](https://bitbucket.org/rowild/pod/commit/6ee8901b7baf90a657b5a1c5d1622985e3588e70))


### Bug Fixes

* add more key event listeners to add new markers ([f5d35dd](https://bitbucket.org/rowild/pod/commit/f5d35dda167f41099b5d01900cc2be9369cee0a5))
* constrain list styling to content and modals ([0a59016](https://bitbucket.org/rowild/pod/commit/0a59016425faf6f5960e3a207c8a42a9f835fd99))
* define certain keyEvents only in experimentStep 2 ([23b67d0](https://bitbucket.org/rowild/pod/commit/23b67d04d798b92509c61afc1a6c1678d5220712))
* first try to fix rootFontSize (WIP) ([e40c584](https://bitbucket.org/rowild/pod/commit/e40c584959de0bd00d8ad11b2d58df20a7d4b289))
* fix height of overview container ([644da99](https://bitbucket.org/rowild/pod/commit/644da99f0d971df873d38795eb6ff54683ada226))
* improve form dependency ([e01a19b](https://bitbucket.org/rowild/pod/commit/e01a19ba775d178c7ab8c0b7e6cabd6bc0a43c67))
* set intro text to centered ([f54d543](https://bitbucket.org/rowild/pod/commit/f54d543e59842e41abd1973905c01e640b31e351))

### [0.0.8](https://bitbucket.org/rowild/pod/compare/v0.0.7...v0.0.8) (2021-12-18)


### Features

* add more keyevents for scaling the waveform and fitting it. ([8028ad9](https://bitbucket.org/rowild/pod/commit/8028ad97942141070074c010fac792f0cf16936e))
* adding symbols to CustomPointMarker (WIP) ([b94478b](https://bitbucket.org/rowild/pod/commit/b94478b465c5ce904feb29b16dcdb2daf93f8c3e))
* Beginning to implement desrciptors ([904533f](https://bitbucket.org/rowild/pod/commit/904533f0f01a81c1f69003b84aeb076f1e3fc130))
* form submission and check implementation, part 1 (WIP) ([4b017da](https://bitbucket.org/rowild/pod/commit/4b017dac2ad7b32e667e7fbf23efc7825f6544df))
* new key event with e.which ([e584017](https://bitbucket.org/rowild/pod/commit/e584017a79485bba30259beebd37e94f2d67dae7))
* new waveform zoom sliders ([6ee8901](https://bitbucket.org/rowild/pod/commit/6ee8901b7baf90a657b5a1c5d1622985e3588e70))


### Bug Fixes

* add more key event listeners to add new markers ([f5d35dd](https://bitbucket.org/rowild/pod/commit/f5d35dda167f41099b5d01900cc2be9369cee0a5))
* constrain list styling to content and modals ([0a59016](https://bitbucket.org/rowild/pod/commit/0a59016425faf6f5960e3a207c8a42a9f835fd99))
* first try to fix rootFontSize (WIP) ([e40c584](https://bitbucket.org/rowild/pod/commit/e40c584959de0bd00d8ad11b2d58df20a7d4b289))
* set intro text to centered ([f54d543](https://bitbucket.org/rowild/pod/commit/f54d543e59842e41abd1973905c01e640b31e351))

### [0.0.7](https://bitbucket.org/rowild/pod/compare/v0.0.6...v0.0.7) (2021-12-14)


### Features

* add instructions button in waveform ([369119e](https://bitbucket.org/rowild/pod/commit/369119e0a1c8f873600b2a1d4b0b67ec1313127a))
* form improvements ([0c50c69](https://bitbucket.org/rowild/pod/commit/0c50c69e695258a4269a0bcd7da6a0b87277b166))
* implement font size changer ([e6d7224](https://bitbucket.org/rowild/pod/commit/e6d7224bc36597f4b303133b187fd9d5ad73dc74))
* remove cues when leaving playground ([939e105](https://bitbucket.org/rowild/pod/commit/939e105b0740ec397453403ebbb54fbd88983327))
* use label text for marker ([5c5ef75](https://bitbucket.org/rowild/pod/commit/5c5ef75360f3a5bebac8fab3907fa182a54f8564))


### Bug Fixes

* add a hex value for color ([162db85](https://bitbucket.org/rowild/pod/commit/162db85b6d5cb64b5d8dc18f83bc85e4c43a9340))
* add db dependency ([f0984f1](https://bitbucket.org/rowild/pod/commit/f0984f1289ecf93a2c24246e612c14242ab0de95))
* db in modals was missing. WIP forms. ([884bc20](https://bitbucket.org/rowild/pod/commit/884bc20a99c2d2426d83a74aee93a4cad208280e))
* switch zoom icons for resizing waveform ([d87ef01](https://bitbucket.org/rowild/pod/commit/d87ef015e80db91e7a2990aec59d4ce878f8b945))

### 0.0.6 (2021-12-13)


### Features

* add pixel-device-recognition ([48dee19](https://bitbucket.org/rowild/pod/commit/48dee19ad1aed31e5d10a2f769b284003fb6c025))
* playground texts, form ([c33a8ef](https://bitbucket.org/rowild/pod/commit/c33a8effb89aabfebc947dca5d3e704e6df40919))
* remove dist from gitignore ([6565b9f](https://bitbucket.org/rowild/pod/commit/6565b9f1c609dbe1b66d9e7bee8298dd0dd61d0a))
* save cues to vuex and indexdb, step 1... WIP ([2958908](https://bitbucket.org/rowild/pod/commit/29589085736050b15a5ab9167e69fc7eaccc4069))
* show warning when spacebar is hit in task 1 ([6e7266e](https://bitbucket.org/rowild/pod/commit/6e7266e218023f979adf4263ea8f3a4878541afe))
* unify url access ([c24111e](https://bitbucket.org/rowild/pod/commit/c24111e2b2944a846be20b0b1e37637ce6576ee3))
* use regular scripts ()build, generate) ([75553a1](https://bitbucket.org/rowild/pod/commit/75553a12745860cf4ae604591bd68f9646ca6cc2))
* waveform loading indicator ([a59625a](https://bitbucket.org/rowild/pod/commit/a59625aae8d5e40ba3941ddf22a6e3ff9a8903ed))


### Bug Fixes

* audio url in prod and dev; call to experimentStep Modals ([8bc2481](https://bitbucket.org/rowild/pod/commit/8bc2481e816d3e418fe450054d86bf1cf810e84c))
* remove assetsUrl ([bec7f53](https://bitbucket.org/rowild/pod/commit/bec7f535a5af530c07a98cc58847ee6f1af63b21))
* remove assetsUrl hoping it wil not be prepended to the files from S3. ([13b8931](https://bitbucket.org/rowild/pod/commit/13b89315cf56ee5370abb8fca7d82b14cd39843c))
* remove curly braces from links. ([c887fe0](https://bitbucket.org/rowild/pod/commit/c887fe048a95c0bdcd2d6a66e6b7e10c11ede2cd))
* rename even more "task" to "step" ([a0214a0](https://bitbucket.org/rowild/pod/commit/a0214a0f59d9b8a7f8a3ff8bc811e2da9f4be1a8))
