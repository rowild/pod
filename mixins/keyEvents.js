export default {
  mounted() {
    this.$nextTick(() => {
      this.initListeners()
    })
    $nuxt.$on('removeKeyEventListeners', _ => this.removeKeyEventListeners())
    $nuxt.$on('addKeyEventListeners', _ => this.initListeners())
  },
  beforeDestroy() {
    // console.log('> Mixin KeyEvents beforeDestroy invoked');
    this.removeKeyEventListeners()
    $nuxt.$off('removeKeyEventListeners')
    $nuxt.$off('addKeyEventListeners')
  },
  methods: {
    initListeners() {
      // KeyboardEvents handler
      // console.log('> Mixin KeyEvents initListeners invoked');
      document.addEventListener('keydown', this.handleKeydownEvents, true);
      document.addEventListener('keyup', this.handleKeyupEvents, true);
    },

    removeKeyEventListeners() {
      // console.log('> Mixin KeyEvents removeKeyEventListeners invoked');
      document.removeEventListener('keydown', this.handleKeydownEvents, true);
      document.removeEventListener('keyup', this.handleKeyupEvents, true);
    },

    // Prevent page from scrolling down, when spacebar is pressed
    // (works only on keydown events!)
    // Only on body, otherwise textareas or input fields are captured, too!
    handleKeydownEvents(e) {
      if(e.key === " " && e.target === document.body) {
        e.preventDefault();
      }
    },

    handleKeyupEvents(e) {
      switch (e.key) {
        // Suppress scrolling with Space bar (usually on key up?)
        case " ": {
          this.experimentStep === 2
            ? this.playPauseAudio()
            : this.$nuxt.$emit('warning', 'In Step 1 you cannot use the spacebar to start playback. Please click the dedicated play button.')
          break
        }
        // Jump to beginning of file with number 0, but only, when not playing
        case "0": {
          // console.log('this.$refs[audio-track] =', this.$refs['audio-track']);
          // console.log('this.audioTrackIsPlaying =', this.audioTrackIsPlaying);
          if (this.experimentStep === 2 && !this.audioTrackIsPlaying && this.$refs['audio-track'].currentTime !== 0) {
            this.$refs['audio-track'].currentTime = 0
          }
          break
        }
        // Add markers
        case "1": {
          this.addNewMarker('1');
          break
        }
        case "2": {
          this.addNewMarker('2');
          break
        }
        case "3": {
          this.addNewMarker('3');
          break
        }
        case "ArrowLeft": {
          if(this.experimentStep === 2) {
            this.zoomInWaveform()
          }
          break
        }
        case "ArrowRight": {
          if(this.experimentStep === 2) {
            this.zoomOutWaveform()
          }
          break
        }
        case "ArrowUp": {
          if(this.experimentStep === 2) {
            this.increaseAmplitude()
          }
          break
        }
        case "ArrowDown": {
          if(this.experimentStep === 2) {
            this.decreaseAmplitude()
          }
          break
        }
        case "+": {
          if(this.experimentStep === 2) {
            this.increaseAmplitude()
          }
          break
        }
        case "-": {
          if(this.experimentStep === 2) {
            this.decreaseAmplitude()
          }
          break
        }
        case ".": {
          if(this.experimentStep === 2) {
            this.fit()
          }
          break
        }
      }
    },
  }
}