export default {
  data: () => {
    return {
      currentFieldsetNumber: 0,
      currentFieldset: 'active-fieldset-0',
    }
  },
  mounted() {
    // console.info('\nMIXIN REGISTRATION-FORM: MOUNTED');
    this.checkFormDependency()
    this.setupRadioBoxes()
  },
  methods: {
    // The "expertise" form block has a dependent form, which only appears, when
    // choosing "university/conservatory"
    // TODO: this is not really finished yet!
    checkFormDependency() {
      this.hideDependency = true
      const x = document.querySelectorAll('input[name="training_level"]')
      const xArr = Array.from(x)

      xArr.forEach(item => {
        item.addEventListener('click', (e) => {
          if (e.target.dataset.level === 'expert') {
            this.hideDependency = false
          }
          else {
            this.hideDependency = true
          }
        })
      })

      const isChecked = x[3].checked
    },

    createAnswerOptions(items) {
      const answerOptions = []

      for (let i = 0; i < items.length; i++) {
        const item = {
          id: 'answer_' + items[i].id,
          label: items[i].answer,
          'data-level': items[i].expertise_level,
          value: this.$slugify(items[i].answer)
        }
        answerOptions.push(item)
      }

      return answerOptions
    },

    getSluggedTxt(text) {
      const sluggedText = this.$slugify(text)
      return sluggedText
    },

    getQuestionIdWithText(id) {
      return 'q_' + id
    },

    setupRadioBoxes() {
      const boxes = document.querySelectorAll('input[name="training_level"]');
      const boxesArr = Array.from(boxes);

      boxesArr.forEach((item) => {
        item.addEventListener("click", (e) => {
          // console.log(e.target.value);
          if (e.target.value === "university_conservatory") {
            this.hideDependentForm = false;
          } else {
            this.hideDependentForm = true;
          }
        });
      });
    },

    // Pagination
    nextFieldset() {
      this.oldFieldset = this.currentFieldset

      this.currentFieldsetNumber < this.fieldsetsTotal.length - 1
        ? this.currentFieldsetNumber++
        : (this.currentFieldsetNumber = 0)

      this.currentFieldset = `active-fieldset-${this.currentFieldsetNumber}`

      this.formWrapper.classList.remove(this.oldFieldset)
      this.formWrapper.classList.add(this.currentFieldset)
    },

    prevFieldset() {
      this.oldFieldset = this.currentFieldset

      this.currentFieldsetNumber !== 0
        ? this.currentFieldsetNumber--
        : (this.currentFieldsetNumber = this.fieldsetsTotal.length - 1)

      this.currentFieldset = `active-fieldset-${this.currentFieldsetNumber}`

      this.formWrapper.classList.remove(this.oldFieldset)
      this.formWrapper.classList.add(this.currentFieldset)
    },
  }
}