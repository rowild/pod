/**
 * Download result mixin
 *
 * Used in Playground, Pilot and Experiment to create a button next to the
 * audio file list (task list). In Playground, registrationData might not yet
 * be available, so take care of that. Also, what is catched with the result
 * variable is not very detailed, when it was saved in "Playground".
 *
 * Used also in the "Save task" modal to also create a download link button.
 */
import db from '@/db/dexie'

export default {
  // computed comes from where this mixin is embedded
  methods: {
    /**
     * Download the task result, that was saved to indexedDB's 'pilot_finished'
     * or 'experiemnt_finished'table, as JSON file
     */
    async downloadResult(params) {
      console.log(' ');
      console.log('DOWNLOAD_RESULT MIXIN downloadResult invoked');
      console.log('   params =', params);
      console.log('   this.getIsExperiment =', this.getIsExperiment);

      let table = params.appSection + '_finished'

      if (params.appSection !== undefined) {
        console.log('   params.appSection =', params.appSection);
        table = params.appSection + '_finished'
      }
      else if (this.appSection !== undefined) {
        console.log('   this.appSection =', this.appSection);
        table = this.appSection + '_finished'
      }
      else {
        console.error('Could not find a valid definition for "appSection"!')
        console.log('   this.appSection =',    this.appSection);
        console.log('   params.appSection =', params.appSection);

        return
      }

      const results = await db.table(table).toArray()
      console.log('   results =', results);

      // This might be too much, since in the local indexedDB there are only
      // tasks from one specific pilot experiment. However...
      // const pilotResults = results.filter(item => item.pilot_id === params.pilotId)
      // console.log('pilotResults =', pilotResults);

      // Get data of previousely saved analysis (was saved when "SaveTask" btn was clikced)
      const result = results.find(item => {
        return item.task_id === parseInt(params.taskFileId)
      })
      // console.log('   result =', result);

      // prepare filename
      let filename = ''

      // Create a filename using the participant id and the task id
      let section = 'playgr-'
      if (this.getIsExperiment && this.getSettings.phase === 'PILOT') section = 'pilot-';
      if (this.getIsExperiment && this.getSettings.phase === 'EXPERIMENT') section = 'pilot-';

      // Get registration data (which might not yet be available in "Playground")
      const regData = await db.registrationData.toArray()
      if (regData && regData.length > 0 && this.getIsExperiment) {
        const participantId = 'partid-' + regData[0].id + '_'
        const sectionId = section + (result.pilot_experiment_id || result.experiment_group_id) + '_'
        const taskId = 'taskid-' + params.taskFileId

        filename = participantId + sectionId + taskId
      } else {
        const participantId = 'partid-4711_'
        const sectionId = section + '42_'
        const taskId = 'taskid-' + params.taskFileId

        filename = participantId + sectionId + taskId
      }

      // Force download
      // Create an anchor element with a `download` attribute, assign the
      // content as a Blob, and eventually perfom a click action on it
      const link = document.createElement('a');
      link.download = filename + '.json';
      const blob = new Blob([JSON.stringify(result, null, 2)], { type: 'text/plain' });
      link.href = window.URL.createObjectURL(blob);
      link.click();
    },
  }
}