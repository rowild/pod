/**
 * This mixin displays a note tht the experiment has to be done untils
 * `created-at` + `grace-period` days.
 */

export default {
  computed: {
    displayGracePeriodNote: {
      get() {
        // console.log('this.getSettings =', this.getSettings);
        const regCreatedDate = new Date(this.regData.created_at)
        // console.log('regCreatedDate =', regCreatedDate);

        const regDay = regCreatedDate.getDate()
        const regMonth = regCreatedDate.getMonth()
        const regFullYear = regCreatedDate.getFullYear()

        const finishDate = new Date(regCreatedDate.setDate(regCreatedDate.getDate() + this.getSettings.grace_period))

        // console.log('finishDate =', finishDate);

        const finishDayName = finishDate.toDateString().split(' ')[0];
        const finishMonth = finishDate.toDateString().split(' ')[1];
        let finishDay = finishDate.toDateString().split(' ')[2];
        const finishYear = finishDate.toDateString().split(' ')[3];

        switch (finishDay) {
          case "1": {
            finishDay += 'st'
            break
          }
          case "2": {
            finishDay += 'nd'
            break
          }
          case "3": {
            finishDay += 'rd'
            break
          }
          default:
            finishDay += 'th'
        }

        const gracePeriodTxt = `Please conduct this experiment until <strong>${finishDayName}, ${finishMonth} ${finishDay}, ${finishYear}</strong>.`

        const now = new Date()
        const nowMS = now.getTime()
        const finishMS = finishDate.getTime()
        const diffMS = finishMS - nowMS
        const diff = Math.floor(diffMS / (1000 * 3600 * 24))

        // console.log('diff =', diff);

        let diffMsg;
        if (diff > this.getSettings.grace_period_warning) {
          diffMsg = `<p>${gracePeriodTxt} (${diff} days remaining.)</p>`
        }
        else if (diff > -1 && diff <= this.getSettings.grace_period_warning) {
          diffMsg = `<p>${gracePeriodTxt} (<span class="text-red-500">${diff} day(s) remaining.</span>)<p>`
        }
        else if (diff < 0) {
          diffMsg = `<p>Grace period expired.</p>`
        }

        // console.log('diffMsg =', diffMsg);

        return diffMsg
      }
    }
  }

}