import { mapGetters } from 'vuex'
import db from '@/db/dexie.js'

const consCol = 'color:darkseagreen'
const consColBold = 'color:darkseagreen;font-weight:bold'
const consColWarn = 'color:darkred;font-weight:bold'

export default {

	computed: {
		...mapGetters('status', ['getSnapshotPilotDetected', 'getSnapshotPilotFromRoute'])
	},

  watch: {
    getSnapshotPilotDetected: {
      async handler(newVal, oldVal) {
        console.group('%cPILOT watch "getSnapshotPilotDetected"', consCol);
        console.log('newVal =', newVal);
        console.log('this.getSnapshotPilotDetected =', this.getSnapshotPilotDetected);
        console.log('this.getSnapshotPilotFromRoute =', this.getSnapshotPilotFromRoute);

        if (newVal) {
          // Check snapshot, if it really has more than 2 cues
          const snapshot = await db.snapshot_pilot.toArray()

          if (snapshot[0] && snapshot[0].cues.length > 2) {
            console.log(' ')
            console.log('------------')
            console.log('Pilot snapshot found')

            $nuxt.$emit('showFeedbackModal', {
              modal_domain: 'GENERAL',
              content_domain: 'RESTORE_SNAPSHOT',
              message: 'Snaphost of a PILOT project was detected',
              message_type: 'warn',
              phase: this.getSettings.phase,
              strict: true
            })

            console.log('------------')
            console.log(' ')
          }
          // if not snapshot was detected, check registration
          else {
            console.log('no snapshot detected');
            // this.$store.commit('status/setSnapshotPilotDetected', false)
          }
        } else {
          console.log('Either initial value on app start or: No snapshot of a Pilot task detected: Continue to Invite Code form.');
        }

        console.groupEnd()
      },
      // We must set immediate to true, otherwise a snapshot will not be detected,
      // when the browser back button is clicked.
      immediate: true
    },
  },

  async mounted() {
    console.log('%cMIXIN detectSnapshotPilot: mounted() invoked', consColBold)
    console.log('%cMIXIN detectSnapshotPilot: this.$route =', consCol, this.$route);

		// Check, if there is a snapshot of an ongoing task available
		const snapshot = await db.snapshot_pilot.toArray()
    console.log('%cMIXIN detectSnapshotPilot: snapshot =', consCol, snapshot);

		if (snapshot[0] && snapshot[0].cues.length > 2) {
			console.log('%cMIXIN detectSnapshotPilot: set snaptshotPilotDetected to true', 'font-weight:bold;color:darkorange');
			this.$store.commit('status/setSnapshotPilotDetected', true)
			this.$store.commit('status/setSnapshotPilotFromRoute', this.$route.name)
		}
    else {
      console.warn('%cMIXIN detectSnapshotPilot: snapshot[0] =', consColWarn, snapshot[0])
      console.log('%cMIXIN detectSnapshotPilot: set snaptshotPilotDetected to false', 'font-weight:bold;color:crimson');
      this.$store.commit('status/setSnapshotPilotDetected', false)
      this.$store.commit('status/setSnapshotPilotFromRoute', this.$route.name)
    }

    console.groupEnd()
  }
}