import { mapGetters } from 'vuex'
import db from '@/db/dexie.js'

const consCol = 'color:darkseagreen'

export default {
  watch: {
    getSnapshotExperimentDetected: {
      async handler(newVal, oldVal) {
        console.log('%cMIXIN detectSnapshotExperiment watch "getSnapshotExperimentDetected", newVal =', consCol, newVal);

        if(newVal) {
          // Check snapshot, if it really has more than 2 cues
          const snapshot = await db.snapshot_experiment.toArray()

          // If snapshot found, show modal
          if(snapshot[0]?.cues.length > 2) {
            $nuxt.$emit('showFeedbackModal', {
              modal_domain: 'GENERAL',
              content_domain: 'RESTORE_SNAPSHOT',
              message: '',
              message_type: 'warn',
              phase: this.getSettings.phase,
              strict: true
            })

          }
        }
      },
      // We must set immediate to true, otherwise a snapshot will not be detected,
      // when the browser back button is clicked.
      immediate: true
    }
  },

	computed: {
		...mapGetters('status', ['getSnapshotExperimentDetected'])
	},

  async mounted() {
    console.log('%cMIXIN detectSnapshotExperiment this.$route =', consCol, this.$route);

		// Check, if there is a snapshot of an ongoing task available
		const snapshot = await db.snapshot_experiment.toArray()
    console.log('%cMIXIN detectSnapshotExperiment mounted: snapshot =', consCol, snapshot);

		if (snapshot[0] && snapshot[0].cues.length > 2) {
			console.log('%cMIXIN detectSnapshotExperiment: set snaptshotExperimentDetected to true', 'color:darkorange');
			this.$store.commit('status/setSnapshotExperimentDetected', true)
			this.$store.commit('status/setSnapshotExperimentFromRoute', this.$route.name)
		}
    else {
      console.warn('snapshot[0] =', snapshot[0])
    }
  }
}