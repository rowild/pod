export default {
  methods: {
    _manageStepOneWaveformLayers() {
      const z = this.getSettings.step1_zoomview_initial_layer_visibility

      // Zoomview

      if (z.show_waveform_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'waveform', 'show')
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'waveform', 'hide')
      }

      if (z.show_time_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'axis', 'show')
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'axis', 'hide')
      }

      if (z.show_marker_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'points', 'show')
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'points', 'hide')
      }

      if (z.show_playhead_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'playhead', 'show')
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'playhead', 'hide')
      }
    },

    _manageStepTwoWaveformLayers() {
      const z = this.getSettings.step2_zoomview_initial_layer_visibility
      const o = this.getSettings.step2_overview_initial_layer_visibility

      if (z.show_waveform_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'waveform', 'show')
        // console.log('Zoomview show waveform layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'waveform', 'hide')
        // console.log('Zoomview show waveform layer: false');
      }

      if (z.show_time_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'axis', 'show')
        // console.log('Zoomview show time layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'axis', 'hide')
        // console.log('Zoomview show time layer: false');
      }

      if (z.show_marker_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'points', 'show')
        // console.log('Zoomview show marker layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'points', 'hide')
        // console.log('Zoomview show marker layer: false');
      }

      if (z.show_playhead_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'playhead', 'show')
        // console.log('Zoomview show playhead layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'zoomview', 'playhead', 'hide')
        // console.log('Zoomview show playhead layer: false');
      }

      // Overview

      if (o.show_waveform_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'waveform', 'show')
        // console.log('Overview show waveform layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'waveform', 'hide')
        // console.log('Overview show waveform layer: false');
      }

      if (o.show_time_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'axis', 'show')
        // console.log('Overview show time layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'axis', 'hide')
        // console.log('Overview show time layer: false');
      }

      if (o.show_marker_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'points', 'show')
        // console.log('Overview show marker layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'points', 'hide')
        // console.log('Overview show marker layer: false');
      }

      if (o.show_playhead_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'playhead', 'show')
        // console.log('Overview show playhead layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'playhead', 'hide')
        // console.log('Overview show playhead layer: false');
      }

      if (o.show_highlight_layer) {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'highlight', 'show')
        // console.log('Overview show highlight layer: true');
      } else {
        $nuxt.$emit('toggleWaveformLayer', 'overview', 'highlight', 'hide')
        // console.log('Overview show highlight layer: false');
      }
    },

    toggleClickTrackVisibility(e) {
      if (this.$refs['click-track-cover-el'].classList.contains('reveal')) {
        this.$refs['click-track-cover-el'].classList.remove('reveal')
      } else {
        this.$refs['click-track-cover-el'].classList.add('reveal')
      }
    },

    toggleWaveforms() {
      // console.log('WAVEFORM METHODS toggleWaveforms this.peaks =', this.peaks)
      if (this.peaksDestroyed) {
        this.initializeStepOne()
        this.peaksDestroyed = false
      } else {
        this.peaks.destroy()
        this.peaksDestroyed = true
      }
    },
  }
}