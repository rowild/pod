/**
 * This mixin checks in various files, whether a participant is registered or
 * not and allows/denies access to the respective site
 */
import { mapGetters } from 'vuex'
import db from "@/db/dexie"

const consCol = 'color:darkcyan; font-weight: bold'

export default {
  data: () => ({
    registered: false,
    participantId: null
  }),
  computed: {
    ...mapGetters('status', ['getSettings', 'getInviteCodeAccepted', 'getIsExperiment']),
  },
  watch: {
    /**
     * If registered is switched to true on mounted() (see mixin), initiate the
     * respective app phase (see "registrationCheck" mixin for script snippets)
     */
    registered(newVal) {
      console.log(' ');
      console.log(':::::::::::');
      console.group('%cMIXIN registrationCheck: watch "registered"', consCol)
      console.log('newVal =', newVal);
      console.log('this.participantId =', this.participantId);
      console.log('this.getInviteCodeAccepted =', this.getInviteCodeAccepted);
      console.log('> this.getSnapshotPilotDetected =', this.getSnapshotPilotDetected);
      console.log('> this.getSnapshotPilotFromRoute =', this.getSnapshotPilotFromRoute);

      if (!this.getSnapshotPilotDetected) {
        console.log('%cNo PILOT snapshot detected, so...', consCol);
        if (newVal && !this.getInviteCodeAccepted) {
          console.log('   > show modal to register');
          $nuxt.$emit('showModalForPilotSubmitCode', {
            id: this.participantId // see mixin!!!
          })
        }
        else {
          console.log('   > loadPilotData (because registration is true & inviteCode has been detected');
          this.loadPilotData({
            participantId: this.participantId,
            pilotId: this.pilotId
          })
        }
      }
      else {
        console.log('%cSnapshot detected. Continue without modal nor loadPilotdata.', consCol);
        // this.getInviteCodeAccepted = true
      }

      console.groupEnd()
      console.log(':::::::::::');
      console.log(' ');
    }
  },
  mounted() {
    setTimeout(() => {
      this.checkIfNotRegistered()
    }, 250)
  },
  methods: {
    async checkIfNotRegistered() {
      console.group('%cMIXIN REGISTRATION CHECK: checkIfNotRegistered invoked', 'color:violet')
      console.log('this.getIsExperiment =', this.getIsExperiment);
      console.log('this.getSettings =', this.getSettings);

      if (this.getIsExperiment) {
        const query = await db.registrationData.toArray()
        console.log('db.registrationData query =', query[0]);

        // If no local registration data can be found, show "Not Registered" modal
        if (query.length < 1) {
          this.registered = false

          $nuxt.$emit('showFeedbackModal', {
            modal_domain: 'REGISTRATION',
            content_domain: 'NOT_REGISTERED',
            message: 'You are not registered.',
            message_type: 'warn',
            phase: this.getSettings.phase,
            strict: true
          })
        }
        else {
          // If, however, local registration data is available, then:
          //   1. Check if this registration is available online
          //   2. If not, offer button to register again || if yes, everything is peachy
          //   3. On concurring, delete local registration
          const appPhase = this.getSettings.phase.toLowerCase() // 'pilot' or 'experiment'

          try {
            // Check (with online strapi backend), if user exists
            const res = await this.$axios.get(`/${appPhase}-participants/${query[0].id}`)
            console.info('User data exists online, so everything is ok, continue without further warning: res =', res);
            this.registered = true
            this.participantId = query[0].id
            this.pilotId = query[0].pilot_id
          }
          catch (error) {
            console.error('You have local registration data, but they could not be found on the server. Probably the grace period within which a project has expired, in which case the registration data gets deleted automatically. You can register again by clicking the button below. In doing so, you delete all PoD related local data and will create them anew.\nerror =', error)

            console.warn('Is it necessary to delete dexie`s db.registrationData?')

            this.registered = false
            $nuxt.$emit('showReRegisterModal', {})
          }
        }
      }
      else {
        console.log('We are not in "experiment" state => no registration data check')
        console.log('this.getSettings.phase =', this.getSettings.phase);
      }

      console.groupEnd()
    },
  }
}