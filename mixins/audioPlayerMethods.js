import Vue from 'vue'
import { mapGetters } from 'vuex'

// MIXIN is used in Waveform

export default {
  data: () => ({
    audioFileDuration: 0,
    audioTrackIsPlaying: false,
    clickTrackIsPlaying: false,
    // Audio player promises
    audioTrackPromise: undefined,
    clickTrackPromise: undefined,
    // For call from Descriptors
    audioTrackOffsetPromise: false
  }),

  computed: {
    audioTrackVolume: {
      get() {
        return this.$store.getters['status/getAudioTrackVolume']
      },
      set(val) {
        this.$store.commit('status/setAudioTrackVolume', val)
      }
    },

    clickTrackVolume: {
      get() {
        return this.$store.getters['status/getClickTrackVolume']
      },
      set(val) {
        this.$store.commit('status/setClickTrackVolume', val)
      }
    }
  },

  mounted() {
    // Event Busses: "soundcontext" is either audio track or click sound
    this.$nuxt.$on('trackAudioSetCurrentTime', (val) => this.trackAudioSetCurrentTime(val))

    this.$nuxt.$on('trackAudioStart', (e, soundContext, offset) => this.trackAudioStart(e, soundContext, offset))
		this.$nuxt.$on('trackAudioPause', (e, soundContext) => this.trackAudioPause(e, soundContext))
    this.$nuxt.$on('trackAudioStop', (e, soundContext) => this.trackAudioStop(e, soundContext))

		this.$nuxt.$on('setAudioTrackVolume', val => {
      this.changeAudioTrackVolume(val)
    })

    this.$nuxt.$on('setClickTrackVolume', val => {
      this.changeClickTrackVolume(val)
    })

    // These methods are in the descriptor overlay (?)
    this.$nuxt.$on('trackAudioOffsetPlay', (val) => this.trackAudioOffsetPlay(val))
    this.$nuxt.$on('trackAudioOffsetStop', (val) => this.trackAudioOffsetStop(val))

    // Listen for the audio file's "ended" event, move to next experimentStep
    this.$refs['audio-track'].onended = () => {
      if (this.audioTrackPromise !== undefined) {
        this.audioTrackPromise
          .then(_ => {
            this.$refs['audio-track'].pause()
            this.$refs['audio-track'].currentTime = 0
            this.audioTrackIsPlaying = false
          })
          .catch(error => {
            console.error('audio track onended error happened, error =', error)
          })
      }
      else {
        console.error('No audioTrackpromise found');
      }

      if (this.experimentStep === 1) {
        this.proceedToNextExperimentStep()
      }
    }
  },

  beforeDestroy() {
    this.$nuxt.$off('trackAudioSetCurrentTime')
    this.$nuxt.$off('trackAudioOffsetPlay')
    this.$nuxt.$off('trackAudioOffsetStop')
    this.$nuxt.$off('trackAudioStart')
    this.$nuxt.$off('trackAudioPause')
    this.$nuxt.$off('trackAudioStop')
    this.$nuxt.$off('setAudioTrackVolume')
    this.$nuxt.$off('setClickTrackVolume')
  },

  methods: {
    /**
     * Audio Player methods
     */
    // called from descriptors via Bus system
    trackAudioSetCurrentTime(val) {
      // console.log('trackAudioSetCurrentTime val =', val);
      this.$refs['audio-track'].currentTime = val
    },

    // called from descriptors via Bus system
    trackAudioOffsetPlay() {
      this.audioTrackOffsetPromise = this.$refs['audio-track'].play()
    },

    // called from descriptors via Bus system
    trackAudioOffsetStop() {
      if (this.audioTrackOffsetPromise !== undefined) {
        this.audioTrackOffsetPromise
          .then(_ => {
            this.$refs['audio-track'].pause()
          })
          .catch(error => {
            console.error('trackAudioOffsetStop error:', error)
          })
      }
    },

    // triggered by space key
    playPauseAudio() {
      if (this.experimentStep === 2) {
        if (this.audioTrackIsPlaying) {
          if (this.audioTrackPromise !== undefined) {
            this.audioTrackPromise
              .then(_ => {
                this.$refs['audio-track'].pause()
                this.audioTrackIsPlaying = false
              })
              .catch(error => {
                console.error('playPauseAudio pause error:', error)
              })
          } else {
            // What now?
          }
        } else {
          this.audioTrackPromise = this.$refs['audio-track'].play()
          this.audioTrackIsPlaying = true
        }
      } else {
        console.warn("You cannot use the Space key for play/pause audio in STEP 1. Please click the dedicated Play button instead.")
      }
    },

    trackAudioStart(e, soundContext, offset = 0) {
      console.log('trackAudioStart invoked... sountContext =', soundContext, ', offset =', offset);
      if (soundContext === 'audioTrack') {
        if(offset > 0) {
          console.log('>>> offset =', offset);
          this.trackAudioSetCurrentTime(offset)
        }

        this.audioTrackPromise = this.$refs['audio-track'].play()
        this.audioTrackIsPlaying = true
        // Since we clicked on the play button we can reset any potential warning
        // caused by hitting the spacekey in step 1
        this.warning = ''
      }
      else {
        this.clickTrackPromise = this.$refs['click-track'].play()
        this.clickTrackIsPlaying = true
      }
    },

    trackAudioPause(e, soundContext) {
      switch (soundContext) {
        case 'audioTrack': {
          if (this.audioTrackPromise !== undefined) {
            this.audioTrackPromise
              .then(_ => {
                this.$refs['audio-track'].pause()
                this.audioTrackIsPlaying = false
              })
              .catch(error => {
                console.error('> audioPlayerMethods.js - error in trackAudioPause promise with audioTrack pause() error:', error)
              })
          }
          else {
            this.$refs['audio-track'].pause()
            console.info('> audioPlayerMethods.js - trackAudioPause: pausing audio track not working, because no Promise was found (probably the audio was never played?)')
          }
          break
        }

        case 'clickTrack': {
          if (this.clickTrackPromise !== undefined) {
            this.clickTrackPromise
              .then(_ => {
                this.$refs['click-track'].pause()
                this.clickTrackIsPlaying = false
              })
              .catch(error => {
                console.error('> audioPlayerMethods.js - trackAudioPause with clickTrack pausing error:', error)
              })
          }
          else {
            this.$refs['click-track'].pause()
            console.info('> audioPlayerMethods.js - trackAudioPause: pausing click track did not find a Promise (probably the audio was never played?)')
          }
          break
        }
      }
    },

    trackAudioStop(e, soundContext) {
      switch (soundContext) {
        case 'audioTrack': {
          if (this.audioTrackPromise !== undefined) {
            this.audioTrackPromise
              .then(_ => {
                this.$refs['audio-track'].pause()
                this.$refs['audio-track'].currentTime = 0
                this.audioTrackIsPlaying = false
              })
              .catch(error => {
                console.error('> audioPlayerMethods.js – trackAudioStop with audioTrack error:', error)
              })
          }
          else {
            console.info('> audioPlayerMethods.js – trackAudioStop: stopping audioTrack did not work, because no promise was detected (probably the audio was never started?)')
          }
          break
        }

        case 'clickTrack': {
          if (this.clickTrackPromise !== undefined) {
            this.clickTrackPromise
              .then(_ => {
                this.$refs['click-track'].pause()
                this.$refs['click-track'].currentTime = 0
                this.clickTrackIsPlaying = false
              })
              .catch(error => {
                console.error('> audioPlayerMethods.js – trackAudioStop with clickTrack error:', error)
              })
          }
          else {
            console.warn('> audioPlayerMethods.js – trackAudioStop: stopping click did not work, because no promise was detected (probably the click was never started?)')
          }
          break
        }
      }
    },

    changeAudioTrackVolume(val) {
      this.audioTrackVolume = val
      this.$refs['audio-track'].volume = val
    },

    changeClickTrackVolume(val) {
      this.clickTrackVolume = val
      this.$refs['click-track'].volume = val
    },

    audioTrackEnded(e) {
      if (this.audioTrackPromise !== undefined) {
        this.audioTrackIsPlaying = false
        // Just in case the offsetPlay called from DescriptorModal is active
        $nuxt.$emit('setIsPlayingToFalseInDescriptorModal')
      }
      else {
        console.warn('audioTrackEnded – no promise defined')
      }
    },

    clickTrackEnded(e) {
      if (this.clickTrackPromise !== undefined) {
        this.clickTrackIsPlaying = false
      }
      else {
        console.warn('clickTrackEnded – no promise defined')
      }
    },
  }
}


/*
// // Alternative method to add audio file to DOM
// const source = document.createElement('source');
// source.setAttribute('src', this.fileData.mediaUrl);
// source.setAttribute('type', 'audio/mp3');
// this.$refs['audio-track'].appendChild(source);

// // TESTS: Audio element eventListener feedback
// this.$refs['audio-track'].addEventListener('loadstart', this.handleEvent);
// this.$refs['audio-track'].addEventListener('canplay', this.handleEvent);
// this.$refs['audio-track'].addEventListener('canplaythrough', this.handleEvent);
// this.$refs['audio-track'].addEventListener('progress', this.handleEvent);
// this.$refs['audio-track'].load()
 */