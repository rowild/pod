export default {
  mounted() {
    $nuxt.$on('deleteOrRestoreMarker', params => this.deleteOrRestoreMarker(params))
    $nuxt.$on('updateActiveColumns', params => this.updateActiveColumns(params))
    $nuxt.$on('updateDescriptorPosition', params => this.updateDescriptorPosition(params))
    $nuxt.$on('updateMarkerDegree', degree => this.updateMarkerDegree(degree))
    $nuxt.$on('updateRemarks', params => this.updateRemarks(params))
    $nuxt.$on('updateSelectedDescriptors', params => this.updateSelectedDescriptors(params))
    // $nuxt.$on('toggleActivatedMarker', params => this.toggleActivatedMarker(params))
  },
  beforeDestroy() {
    $nuxt.$off('deleteOrRestoreMarker')
    $nuxt.$off('updateActiveColumns')
    $nuxt.$off('updateDescriptorPosition')
    $nuxt.$off('updateMarkerDegree')
    $nuxt.$off('updateRemarks')
    $nuxt.$off('updateSelectedDescriptors')
    // $nuxt.$off('toggleActivatedMarker')
  },
  methods: {
    /**
     * Update the marker degree.
     * Requires to set more options (like textField, height, color...)
     *
     * @param {object} params   Hold the id and the new deegree
     */
    updateMarkerDegree(params) {
      // console.log('updateMarkerDegree params =', params);
      // update point (should happen as reaction to vuex update)
      const currPoint = this.peaks.points.getPoint(params.id)
      currPoint.update({
        markerDegree: params.degree,
        labelText: "" + params.degree,
        pointerHeight: this.pointHeights[params.degree],
        color: this.pointColors[2][params.degree]
      })

      // update vuex
      this.$store.commit('experiment/cueUpdate', {
        id: params.id,
        markerDegree: params.degree,
        labelText: "" + params.degree, // must be of type String
        pointerHeight: this.pointHeights[params.degree],
        color: this.pointColors[2][params.degree]
      })
    },

    /**
     * Set the "deleted" flag on a marker
     * Update the visual display via CustomMarkers
     *
     * @param {object} params   Holds the id of the current marker
     */
    deleteOrRestoreMarker(params) {
      // update point (should happen as reaction to vuex update)
      const currPoint = this.peaks.points.getPoint(params.id)
      currPoint.update({
        deleted: params.setDeletedFlag,
        draggable: !params.setDeletedFlag
      })

      // update vuex
      this.$store.commit('experiment/cueUpdate', {
        id: params.id,
        deleted: params.setDeletedFlag,
        draggable: !params.setDeletedFlag
      })
    },

    /**
     * Update Descriptor Position selection
     *
     * @param {array} params   [0,0] The first array posiiton is for "before", the
     *                         second for "after" descriptorPosition areas
     */
    updateDescriptorPosition(params) {
      // console.log('updateDescriptorPosition params =', params);
      const currPoint = this.peaks.points.getPoint(params.id)

      if (params.position === 'before') {
        // update peaks point directly (should happen as reaction to vuex update)
        currPoint.update({
          descriptorBeforeActive: params.value,
          validated: params.validated
        })

        // update vuex
        this.$store.commit('experiment/cueUpdate', {
          id: params.id,
          descriptorBeforeActive: params.value,
          validated: params.validated
        })
      }
      else {
        currPoint.update({
          descriptorAfterActive: params.value,
          validated: params.validated
        })

        // update vuex
        this.$store.commit('experiment/cueUpdate', {
          id: params.id,
          descriptorAfterActive: params.value,
          validated: params.validated
        })
      }
    },

    /**
     * Update Selected Descriptors
     *
     * @param {array} params   An array that hold up to three integers (as strings)
     *                         indicating the descriptor that is assigned
     *                         (129, 239, 105), where 1xx is for "before" and
     *                         2xx is for "after" descriptors
     */
    updateSelectedDescriptors(params) {
      // console.log('\nSTORE/EXPERIMENT/updateSelectedDescriptors: params =', params);
      // update peaks point directly (should happen as reaction to vuex update)
      const currPoint = this.peaks.points.getPoint(params.id)
      currPoint.update({
        descriptors: params.descriptors,
        validated: params.validated,
        itemsInBeforeCol: params.itemsInBeforeCol,
        itemsInAfterCol: params.itemsInAfterCol,
      })

      // update vuex
      this.$store.commit('experiment/cueUpdate', {
        id: params.id,
        descriptors: params.descriptors,
        validated: params.validated,
        itemsInBeforeCol: params.itemsInBeforeCol,
        itemsInAfterCol: params.itemsInAfterCol,
      })
    },

    updateRemarks(params) {
      // console.log('updateRemarks params =', params);
      // update peaks point directly (should happen as reaction to vuex update)
      const currPoint = this.peaks.points.getPoint(params.id)
      currPoint.update({
        remarks: params.remarks
      })

      // update vuex
      this.$store.commit('experiment/cueUpdate', {
        id: params.id,
        remarks: params.remarks
      })
    },

    updateActiveColumns(params) {
      // console.log('updateActiveColumns params =', params);
      // update peaks point directly (should happen as reaction to vuex update)
      const currPoint = this.peaks.points.getPoint(params.id)
      currPoint.update({
        activeColumns: params.activeColumns
      })

      // update vuex
      this.$store.commit('experiment/cueUpdate', {
        id: params.id,
        activeColumns: params.activeColumns
      })
    },


    /**
     * Set the "activated" flag on a marker
     * Update the visual display via CustomMarkers
     *
     * @param {object} params   Holds the id of the current marker
     */
    /*
      toggleActivatedMarker(params) {
        // console.log('toggleActivatedMarker ivoked: params =', params);
        // // update point (should happen as reaction to vuex update)
        // const currPoint = this.peaks.points.getPoint(params.id)
        // currPoint.update({
        //   activated: params.activated,
        // })

        // update vuex - needed here?
        // this.$store.commit('experiment/cueUpdate', {
        //   id: params.id,
        //   activated: params.activated,
        // })
    },
    */
  }
}