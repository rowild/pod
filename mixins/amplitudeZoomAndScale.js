
const consCol = 'font-weight:bold;color:blue'
const consColWarn = 'font-weight: bold; color: crimson;'

export default {
  data: () => ({
    /**
     * Since we set the steps of the input range slider according to the number
     * of zoomLevels, these values must be reactive (in order to get the length
     * of the array - currently this is set manually... not even computed works...)
     * Also needed for assigning the max value of the respective input[range] element
     */
    currZoomLevelIsSetToFit: true,
    activeZoomLevel: 5,

    // Also needed for assigning the max value of the respective input[range] element
    amplitudeScales: {
      "0": 0.0,
      "1": 0.1,
      "2": 0.25,
      "3": 0.5,
      "4": 0.75,
      "5": 1.0,
      "6": 1.5,
      "7": 2.0,
      "8": 3.0,
      "9": 4.0,
      "10": 5.0,
      "11": 6.0,
      "12": 7.0,
      "13": 8.0,
      "14": 9.0,
      "15": 10.0
    },

    // 0 - 10; 5 = 1.0 ("5" is the "key")
    currentAmplitudeScaleKey: 5,

    // 0 - 10; 5 = 1.0 ("1.0" is the value associated with key 5)
    currentAmplitudeScaleVal: 1.0,
  }),
  computed: {
    getAmplitudeScalesLength() {
      // console.log('Object.keys(this.amplitudeScales).length =', Object.keys(this.amplitudeScales).length);
      return Object.keys(this.amplitudeScales).length - 1
    },
  },
  mounted() {
    // console.log(' ');
    // console.group('%cMIXIN amplitudeZoomAndScale - zoomInWavefrom() invoked', 'color:crimson')

    window.addEventListener('resize', () => {
      console.log('%c... RESIZING ... ', consCol);
      console.log('Remember to invoke this also from font resiizer!!!');
      this.resizeCanvas()
    })

    // console.groupEnd()
    // console.log(' ');
  },
  methods: {
    toggleFitZoom() {
      console.log('this.zoomLevels[this.activeZoomLevel] =', this.zoomLevels[this.activeZoomLevel]);
      if (!this.currZoomLevelIsSetToFit) {
        this.zoomview.setZoom({ scale: 'auto' })
        this.currZoomLevelIsSetToFit = true
        // this.$refs['waveform-zoom-slider'].value = this.zoomLevels.length
        // increase / decrease waveform
        this.zoomview.setAmplitudeScale(this.amplitudeScales[5])
      }
      else {
        this.currZoomLevelIsSetToFit = false
        // Set to previous zoom value
        this.zoomview.setZoom({
          scale: this.zoomLevels[this.activeZoomLevel]
        })
      }

      console.log('this.activeZoomLevel =', this.activeZoomLevel);
    },

    /* -------------- ZOOM ------------- */

    onZoomSliderInput(e) {
      console.log(' ');
      console.group('%cMIXIN amplitudeZoomAndScale - onZoomSliderInput() invoked', 'color:crimson')
      console.log('e.target.value =', e.target.value);
      this.peaks.zoom.setZoom(e.target.value)
      this.$refs['waveform-zoom-slider'].value = e.target.value
      this.currZoomLevelIsSetToFit = false
      this.activeZoomLevel = this.peaks.zoom.setZoom(e.target.value)
      console.log('onZoomSliderInput this.activeZoomLevel =', this.activeZoomLevel);
      console.groupEnd()
      console.log(' ');
    },

    zoomInWaveform() {
      console.log(' ');
      console.group('%cMIXIN amplitudeZoomAndScale - zoomInWavefrom() invoked', 'color:crimson')
      this.peaks.zoom.zoomIn();
      console.log('zoomInWaveform this.peaks.zoom.getZoom() =', this.peaks.zoom.getZoom());
      this.$refs['waveform-zoom-slider'].value = this.peaks.zoom.getZoom()
      this.currZoomLevelIsSetToFit = false
      this.activeZoomLevel = this.peaks.zoom.getZoom()
      // console.log('this.peaks.zoom.getZoomLevel() NEWLY SET=', this.peaks.zoom.getZoomLevel());
      console.log('zoomInWaveform this.activeZoomLevel =', this.activeZoomLevel);
      console.log('zoomInWaveform this.currZoomLevelIsSetToFit =', this.currZoomLevelIsSetToFit);
      console.groupEnd()
      console.log(' ');
    },

    zoomOutWaveform() {
      console.log(' ');
      console.group('%cMIXIN amplitudeZoomAndScale - zoomOutWavefrom() invoked', 'color:crimson')
      this.peaks.zoom.zoomOut();
      console.log('zoomOutWaveform this.peaks.zoom.getZoom() =', this.peaks.zoom.getZoom());
      this.$refs['waveform-zoom-slider'].value = this.peaks.zoom.getZoom()
      this.currZoomLevelIsSetToFit = false
      this.activeZoomLevel = this.peaks.zoom.getZoom()
      console.log('zoomOutWaveform this.activeZoomLevel =', this.activeZoomLevel);
      console.log('zoomOutWaveform this.currZoomLevelIsSetToFit =', this.currZoomLevelIsSetToFit);
      console.groupEnd()
      console.log(' ');
    },

    /* -------------- AMPLPITUDE ------------- */

    onAmplitudeSliderInput(e) {
      // console.log('Wavefrom.vue mixin amplitudeZoomAndScale: onAmplitudeSliderINput invoked');
      this.zoomview.setAmplitudeScale(this.amplitudeScales[e.target.value]);
      this.currentAmplitudeScaleKey = parseInt(e.target.value)
      this.$refs['amplitude-scale-slider'].value = this.currentAmplitudeScaleKey
      this.currZoomLevelIsSetToFit = false
    },

    increaseAmplitude(e) {
      // console.log('Wavefrom.vue mixin amplitudeZoomAndScale: increaseAmplitude invoked');
      if (this.currentAmplitudeScaleKey < this.getAmplitudeScalesLength) {
        this.currentAmplitudeScaleKey += 1
        const scaleVal = this.amplitudeScales[this.currentAmplitudeScaleKey]

        if (scaleVal !== undefined) {
          this.zoomview.setAmplitudeScale(scaleVal)
          this.$refs['amplitude-scale-slider'].value = this.currentAmplitudeScaleKey
        } else {
          this.currentAmplitudeScaleKey = 5
        }
      } else {
        console.log('amplitudeZoomAndScale mixin: Amplitude maximum size reached');
      }
      this.currZoomLevelIsSetToFit = false
    },

    decreaseAmplitude(e) {
      // console.log('Wavefrom.vue mixin amplitudeZoomAndScale: decreaseAmplitude invoked');
      if (this.currentAmplitudeScaleKey > 0) {
        this.currentAmplitudeScaleKey -= 1
        const scaleVal = this.amplitudeScales[this.currentAmplitudeScaleKey]

        if (scaleVal !== undefined) {
          this.zoomview.setAmplitudeScale(scaleVal)
          this.$refs['amplitude-scale-slider'].value = this.currentAmplitudeScaleKey
        } else {
          console.log('amplitudeZoomAndScale mixin: scaleVal was undefined, probably due to a "dirty" this.currentAmplitudeScaleKey: ', this.currentAmplitudeScaleKey);
          console.log('amplitudeZoomAndScale mixin: Resetting to initial value 5:');
          this.currentAmplitudeScaleKey = 5
        }
      } else {
        console.log('amplitudeZoomAndScale mixin: Amplitude minimum size reached');
      }
      this.currZoomLevelIsSetToFit = false
    },

    /* ----------------------- OTHER -------------------- */

    resizeCanvas() {
      console.log('   resizeCanvas() invoked');
      const zoomviewContainer = this.$refs.zoomview
      const overviewContainer = this.$refs.overview
      console.log('    overviewContainer =', overviewContainer);

      const zoomviewStyle = 'width: 100%;'
      const overviewStyle = 'width: 100%;'

      zoomviewContainer.setAttribute('style', zoomviewStyle);
      overviewContainer.setAttribute('style', overviewStyle);

      const zoomview = this.peaks.views.getView('zoomview');
      if (zoomview && this.currZoomLevelIsSetToFit) {
        console.log('this.currZoomLevelIsSetToFit =', this.currZoomLevelIsSetToFit);
        zoomview.fitToContainer();
      }
      else {
        console.log('Do not adjust size of zoomview! this.currZoomLevelIsSetToFit =', this.currZoomLevelIsSetToFit);
      }

      const overview = this.peaks.views.getView('overview');
      if (overview) {
        overview.fitToContainer();
      }
    }
  }
}