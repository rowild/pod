import Vue from 'vue'
import VueSimpleContextMenu from 'vue-simple-context-menu'

Vue.component('VueSimpleContextMenu', VueSimpleContextMenu)

const consCol = 'color:darkseablue'
const consColWarn = 'color:crimson'

export default {
  data: () => ({
    peaksPoint: {}
  }),
  computed: {
    options() {
      const options = []

      options.push({
        name: 'Delete / re-activate',
        slug: 'delete-reactivate-marker',
        class: 'text-sm cursor-pointer'
      })

      options.push({
        type: 'divider'
      })

      options.push({
        name: 'Change to 1',
        slug: 'change-marker-degree-to-1',
        class: (this.peaksPoint.markerDegree === 1 || this.peaksPoint.deleted) ? 'text-sm pointer-events-none text-gray-400 cursor-is-not-allowed' : 'text-sm cursor-pointer'
      })

      options.push({
        name: 'Change to 2',
        slug: 'change-marker-degree-to-2',
        class: (this.peaksPoint.markerDegree === 2  || this.peaksPoint.deleted) ? 'text-sm pointer-events-none text-gray-400 cursor-is-not-allowed' : 'text-sm cursor-pointer'
      })

      options.push({
        name: 'Change to 3',
        slug: 'change-marker-degree-to-3',
        class: (this.peaksPoint.markerDegree === 3 || this.peaksPoint.deleted) ? 'text-sm pointer-events-none text-gray-400 cursor-is-not-allowed' : 'text-sm cursor-pointer'
      })

      options.push({
        type: 'divider'
      })

      options.push({
        name: 'Descriptors...',
        slug: 'open-descriptor-modal',
        class: this.peaksPoint.deleted ? 'text-sm pointer-events-none text-gray-400 cursor-is-not-allowed' : 'text-sm cursor-pointer'
      })

      return options
    }
  },
  mounted() {
    // Supress right click on dropdown menu
    const cmEl = document.querySelector('.vue-simple-context-menu')
    cmEl.addEventListener('contextmenu', (e) => {
      e.preventDefault()
      e.stopImmediatePropagation()
      e.stopPropagation()
    })
  },
  methods: {
    /**
     * This function is called from a $nuxt.$on BUS that is called from the
     * a peaks point's `click` vent, when `button === 2`(= rightclick) was
     * detected.
     *
     * @param {MouseEvent|PointerEvent} event Depending where the event is coming
     * 			from it could be a PointerEvent (from an DOM element) or a MoouseEvent
     * 			(from a peaks marker)
     * @param {*} item Depending where function call is coming from this is either
     * 			a custom item (defined according to the ext's definition) or a Peaks
     * 			Point.
     */
    handleClick(event, peaksPoint) {
      this.peaksPoint = peaksPoint
      this.$refs.vueSimpleContextMenu.showMenu(event, peaksPoint)
    },

    /**
     * A function called from an item in a contextmenu
     *
     * @param {Object} obj Consists of `obj.option` (which is the object defined
     * 		in the `data.options` parameter for this element) and `obj.item` (which
     * 		is a Peaks point).
     */
    optionClicked(obj) {
      // obj.item, obj.point
      // console.log('optionClicked obj =', obj);

      switch (obj.option.slug) {
        case 'delete-reactivate-marker': {
          // obj that holds only the data that needs to be updated
          const newDataObj = {
            clickComingFrom: 'contextmenu',
            id: obj.item.id,
            setDeletedFlag: !obj.item.deleted
          }

          $nuxt.$emit('deleteOrRestoreMarker', newDataObj)

          break
        }

        case 'change-marker-degree-to-1': {
          $nuxt.$emit('updateMarkerDegree', {
            degree: 1,
            id: obj.item.id
          })

          break
        }

        case 'change-marker-degree-to-2': {
          $nuxt.$emit('updateMarkerDegree', {
            degree: 2,
            id: obj.item.id
          })
          break
        }

        case 'change-marker-degree-to-3': {
          $nuxt.$emit('updateMarkerDegree', {
            degree: 3,
            id: obj.item.id
          })
          break
        }

        case 'open-descriptor-modal': {
          $nuxt.$emit('showModalForDescriptors', obj.item)
          break
        }

        default: {
          console.log('no option selected')
        }
      }
    },

    /**
     * Subpress default rightclick on the popup menu itself
     * @param {Object} e
     */
    handleRightClick(e) {
      console.log('prevent right click on contextmenu popup itself, e =', e);
    }
  }
}