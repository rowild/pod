import { mapGetters } from 'vuex'
import db from '@/db/dexie.js'

const consCol = 'color:darkseagreen'

export default {
  watch: {
    getSnapshotPlaygroundDetected: {
      async handler(newVal, oldVal) {
        console.log('%cMIXIN detectSnapshotPlayground watch "getSnapshotDetected", newVal =', consCol, newVal);
        if(newVal) {
          // Check snapshot, if it really has more than 2 cues
          const snapshot = await db.snapshot_playground.toArray()
          if(snapshot[0]?.cues.length > 2) {
            $nuxt.$emit('showFeedbackModal', {
              modal_domain: 'GENERAL',
              content_domain: 'RESTORE_SNAPSHOT',
              message: 'Snapshot of a PLAYGROUND task detected',
              message_type: 'warn',
              phase: this.getSettings.phase,
              section: 'playground',
              strict: true
            })
          }
        }
      },
      // We must set immediate to true, otherwise a snapshot will not be detected,
      // when the browser back button is clicked.
      immediate: true
    }
  },

	computed: {
		...mapGetters('status', ['getSnapshotPlaygroundDetected'])
	},

  async mounted() {
    console.group('%cMIXIN detectSnapshotPlayground mounted() invoked', consCol)
    console.log('this.$route =', this.$route);

		// Check, if there is a snapshot of an ongoing task available
		const snapshot = await db.snapshot_playground.toArray()
    console.log('snapshot =', consCol, snapshot);

		if (snapshot[0] && snapshot[0].cues.length > 2) {
			console.log('%cset snapShotDetected to true', 'color:darkorange;font-weight:bold;');
			this.$store.commit('status/setSnapshotPlaygroundDetected', true)
			this.$store.commit('status/setSnapshotPlaygroundFromRoute', this.$route.name)
		}
    else {
      console.warn('snapshot[0] =', snapshot[0])
      console.log('%cset snaptshotDetected to false', 'color:crimson;font-weight:bold;');
      this.$store.commit('status/setSnapshotPilotDetected', false)
      this.$store.commit('status/setSnapshotPilotFromRoute', this.$route.name)
    }

    console.groupEnd()
  }
}