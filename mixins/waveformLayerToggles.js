/**
 * All these events are emitted from "component/peaks/PeaksWaveformControls.vue"
 */
export default {
  data: () => ({
    zoomviewToggles: {
      axis: {
        toggle: 'AxisLayerVisible',
        status: true,
        domElemToShowOrHide: '_axisLayer'
      },
      playhead: {
        toggle: 'PlayheadVisible',
        status: true,
        domElemToShowOrHide: '_playheadLayer',
        additionalPathSegment: '_playheadGroup'
      },
      points: {
        toggle: 'PointsLayerVisible',
        status: true,
        domElemToShowOrHide: '_pointsLayer',
        additionalPathSegment: '_layer'
      },
      waveform: {
        toggle: 'WaveformLayerVisible',
        status: true,
        domElemToShowOrHide: '_waveformLayer'
      }
    },
    overviewToggles: {
      axis: {
        toggle: 'AxisLayerVisible',
        status: true,
        domElemToShowOrHide: '_axisLayer'
      },
      highlight: {
        toggle: '',
        status: true,
        domElemToShowOrHide: '_highlightLayer',
        additionalPathSegment: '_layer'
      },
      playhead: {
        toggle: 'PlayheadVisible',
        status: true,
        domElemToShowOrHide: '_playheadLayer',
        additionalPathSegment: '_playheadGroup'
      },
      points: {
        toggle: 'PointsLayerVisible',
        status: true,
        domElemToShowOrHide: '_pointsLayer',
        additionalPathSegment: '_layer'
      },
      waveform: {
        toggle: 'WaveformLayerVisible',
        status: true,
        domElemToShowOrHide: '_waveformLayer'
      }
    },
  }),
  mounted() {
    // console.warn('MIXIN waveformLayerTogglesMixin MOUNED()');
    this.$nuxt.$on('toggleWaveformLayer', (view, currentLayer, layerVisibility = 'auto') => {
      // console.log('   view:', view, ', currentLayer:', currentLayer, '->', layerVisibility, ' (layer visibility))
      // console.log('     view =', view);
      // console.log('     currentLayer =', currentLayer);
      const obj = this[view + 'Toggles'][currentLayer]
      const l = obj.domElemToShowOrHide
      if (layerVisibility === 'auto') {
        // console.log('obj.status =', obj.status);
        if (obj.status) {
          this.hideLayer(obj, view, l)
        } else {
          this.showLayer(obj, view, l)
        }
      } else if (layerVisibility === 'show') {
        this.showLayer(obj, view, l)
      } else if (layerVisibility === 'hide') {
        this.hideLayer(obj, view, l)
      }
    })
  },
  methods: {
    /**
     * Not all paths to a peaks layer are build with the same elements;
     * some have additional path elements, which we call "additionalPathSegments"
     * here. It is obligatory to check agaist them separately!
     *
     * @param {object} obj
     * @param {string} view Overview or Zoomview
     * @param {string} layer Layer to show or hide
     */
    showLayer(obj, view, layer) {
      if (obj.additionalPathSegment) {
        this[view][layer][obj.additionalPathSegment].show()
      } else {
        this[view][layer].show()
      }
      obj.status = true
    },
    hideLayer(obj, view, layer) {
      if (obj.additionalPathSegment) {
        this[view][layer][obj.additionalPathSegment].hide()
      } else {
        this[view][layer].hide()
      }
      obj.status = false
    }
  },
  beforeDestroy() {
    $nuxt.$off('toggleWaveformLayer')
  }
}