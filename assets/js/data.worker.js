import db from '@/db/dexie.js'

const consCol = 'color:darkgreen'
const consColSimple = 'color:green'
const consColWarn = 'color:crimson'

// 'taskCues' is payload sent via `worker.postMessage in peaks.vue - methods - backupCurrentTask`
onmessage = async (payload) => {
  console.log('%cWORKER - payload.data =', consCol, payload.data);

  // table to save to
  const tbl = db[`snapshot_${payload.data.section}`]
  console.log('%c   > worker: tbl =', consColSimple, tbl);

  switch (payload.data.action) {
    case 'save': {
      console.log('%c   > worker: Switch(action): save: Create snapshot (backup of current VueX)', consColSimple);
      console.log('%c   > worker: SAVE payload.data.action =', consColSimple, payload.data.action);
      console.log('%c   > worker: SAVE payload.data.state =', consColSimple, payload.data.state);

      /**
       * Override id and set it to 0 in order to secure:
       * 1. an initial creation of the data in case the DB strucutre does not exist
       * 2. to always override "id: 0" instead of creating new entries
       */

      payload.data.state.id = parseInt(0)

      // await db.snapshot.delete(parseInt(0))
      await tbl.put(payload.data.state, { id: 0 })
      console.log(`%c   > worker: put payload.data.state ${payload.data.state} to tbl ${tbl}`, consColSimple);


      // Make sure clickTrackVolume is not undefined
      const clickVol = payload.data.clickTrackVolume !== undefined ? payload.data.clickTrackVolume : 0.35
      console.log('%c   > worker: reset click volume', consColSimple);

      await db.status.update(0, {
        audioTrackVolume: payload.data.audioTrackVolume,
        clickTrackVolume: clickVol
      })
      console.log('%c   > worker: update "status" autioTrackVoume and clickTrackColume', consColSimple);

      postMessage({
        backupSaved: true
      })
      console.log('%c   > worker: postMessage "backupSaved: true"', consColSimple);

      break
    }

    /**
     * KEEP IN MIND:
     * When saveTask is executed, it deletes the snapshot in its own process.
     * Otherwise the redirect happens too fast and the app would still recognize
     * a snapshot
     */
    case 'delete': {
      console.log('%cSwitch (action): delete: Delete snapshot (backup of current VueX)', consColWarn);

      await tbl.update(0, {
        activeAudio: {},
        combinedCues: {},
        cues: {},
        cuesOfStepOne: {},
        cuesOfStepTwo: {},
        descriptors: {},
        feedbackMessage: ''
      })
      console.log(`%c   > worker: update tbl ${tbl} (reset things like activeAudio, cues, descriptors etc to empty objects, "{}")`, consColWarn);

      await db.status.update(0, {
        audioTrackVolume: 0.5,
        clickTrackVolume: 0.5
      })
      console.log(`%c   > worker: update "status" (reset audio and click track volumes)`, consColWarn);

      postMessage({
        deletedBackup: true
      })
      console.log('%c   > worker: postMessage "deletedBackup: true"', consColWarn);

      break
    }
  }

  // const usersObj = await import(`@/data/users.json`)
  // const users = Object.values(usersObj)
  // const usersWithId = users.map((u, id) => ({ ...u, id }))
  // await db.contents.bulkPut(usersWithId)

  // postMessage({
  //   loaded: true,
  // })
}