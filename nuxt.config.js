// Won't work, because that's rendered on the server...
// const scale = 1 / window.devicePixelRatio

export default {
  loading: {
    color: 'gray',
    height: '2px'
  },
  ssr: false,
  target: 'server',
  dev: process.env.NODE_ENV !== "production",
  debug: process.env.NODE_ENV !== "production",

  // @see https://nuxtjs.org/tutorials/moving-from-nuxtjs-dotenv-to-runtime-config/
  privateRuntimeConfig: {
    apiSecret: process.env.API_SECRET
  },
  publicRuntimeConfig: {
    baseURL: process.env.NODE_ENV === 'production' ? process.env.BASE_URL : process.env.BASE_URL_LOCAL,
    axios: {
      browserBaseURL: process.env.NODE_ENV === 'production' ? process.env.STRAPI_API : process.env.STRAPI_API_LOCAL
    }
  },

  layoutTransition: {
    name: 'fadescale',
    mode: 'out-in',
    appear: true
  },

  pageTransition: {
    name: 'fade',
    mode: 'out-in',
    appear: true
  },

  server: {
    port: 8000
  },

  serverMiddleware: [
    // '~/middleware/redirects.js'
    '~/middleware/headers.js'
  ],

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | Points of Discontinuity',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'An audio experiment created by Dr. Christian Utz at the University of Graz with support by the FWF.' },
      { name: 'format-detection', content: 'telephone=no' },
      { 'http-equiv': 'pragma', content: 'no-cache' },
      { 'http-equiv': 'cache-control', content: 'no-cache' },
      { 'http-equiv': 'expires', content: '0' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;700&family=Open#Sans#Condensed:wght@300;400&display=swap' }
    ],
    script: []
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/css/variables.css",
    "@/assets/css/layout.css",
    "@/assets/css/typography.css",
    "@/assets/css/lists.css",
    "@/assets/css/navigation.css",
    "@/assets/css/sidebar.css",
    "@/assets/css/form.css",
    "@/assets/css/formulate.css",
    "@/assets/css/link-underlined.css",
    "@/assets/css/peaks.css",
    "@/assets/css/key-colors.css",
    "@/assets/css/tile.css",
    "@/assets/css/animations.css",
    "@/assets/css/scrollbar.css",
    "@/assets/css/topic.css",
    "@/assets/css/vue-final-modal.css",
    "@/assets/css/vue-simple-context-menu.css",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/vue-final-modal.js',
      // ssr: false
    },
    {
      src: '~/plugins/slugify.js',
      // ssr: false
    },
    {
      src: '~/plugins/peaks.client.js',
      ssr: false
    },
    {
      src: '~/plugins/inject-ww.js',
      ssr: false
    },
    // {
    //   src: '~/plugins/ki-context.js',
    //   ssr: false
    // },
    // {
    //   src: '~/plugins/vue-simple-context-menu.js',
    //   ssr: false
    // }
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@braid/vue-formulate/nuxt',
    // '@nuxtjs/color-mode'
  ],

  formulate: {
    configPath: '~/configs/formulate.config.js'
  },

  gsap: {
    /* module options */
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/strapi',
    // https://github.com/vaso2/nuxt-fontawesome
    [
      'nuxt-fontawesome',{
        imports: [
          {
            set:'@fortawesome/free-brands-svg-icons',
            icons: ['fab']
          },
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          }
        ]
      }
    ],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  // See above at publicRuntimeConfig for urls URLs with env variables
  // @see https://axios.nuxtjs.org/options/
  axios: {
    // baseURL: process.env.NODE_ENV === 'production' ? process.env.STRAPI_API : process.env.STRAPI_API_LOCAL, // not working
    // baseURL: process.env.NODE_ENV === 'production' ? 'https://strapi-pod.herokuapp.com' : 'http://localhost:1337',
    baseURL: 'http://0.0.0.0:1337' // fallback
  },

  strapi: { },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      // order: ['postcss-import', 'postcss-preset-env', 'cssnano'],
      plugins: {
        "postcss-preset-env": {
          stage: 0,
          importFrom: "./assets/css/variables.css"
        }
      }
    },
    transpile: [ 'vue-final-modal' ],

    loaders: {
      vue: {
        transformAssetUrls: {
          audio: 'src',
          source: 'src',
        },
        esModule: false,
      },
      file: { esModule: false },
    },

    extend(config, ctx) {
      // console.log('config =', config);
      // console.log('ctx =', ctx);
      // console.log('ctx.isClient =', ctx.isClient);

      config.module.rules.push({
        test: /\.(ogg|mp3|m4a|aac|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
        },
      })

      // Needed for web worker
      config.output.globalObject = 'this'

      if (ctx.isClient) {
        config.module.rules.push({
          test: /\.worker\.js$/,
          loader: 'worker-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },

  router: {
    base: '/',
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'not-found',
          path: '*',
          component: resolve(__dirname, 'pages/404.vue')
        }
      )
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },
}
