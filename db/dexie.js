import Dexie from 'dexie'
const db = new Dexie('pod_project_db');

const getDevicePixelRatioDependentFontSize = () => {
    if (window.devicePixelRatio <= 1) {
        return 18
    }
    if (window.devicePixelRatio > 1 && window.devicePixelRatio <= 1.25) {
        return 16
    }
   if (window.devicePixelRatio > 1.25 && window.devicePixelRatio <= 1.5) {
        return 14
    }
    if (window.devicePixelRatio > 1.5 && window.devicePixelRatio <= 1.75) {
        return 12
    }
    if (window.devicePixelRatio > 1.75 && window.devicePixelRatio <= 2) {
        return 12
    }
    if (window.devicePixelRatio > 2 && window.devicePixelRatio <= 2.5) {
        return 12
    }
    if (window.devicePixelRatio > 2.5 && window.devicePixelRatio <= 4) {
        return 12
    }
}

/**
 * Object stores that have "_info" appended:
 * Infos that are important in the context of handling all audio files: update
 * it, when an analysis has been completed; once all analyses are completed,
 * set "analyses_complete" to true. That way we can check one variable instead
 * of fetching all entries, getting all their analysed status and the array length
 * and comparing those.
 * Also, hold a reference to the mediaId of already analysed adio file. Use it
 * when the refresh button is clicked (only in development? test and pilot?) to
 * reset each audio file's "analysed" property (wich "refresh" overrides)
 */
db.version(5).stores({
    status: 'id++',
    registrationData: 'id++', // Registration data of a participant
    // Modal Texts
    modals: 'id++,type',
    contents: 'id++,type',
    // Descriptors
    descriptors: 'id',
    descriptor_texts: 'id',
    descriptor_placement_options: 'id',
    // Experiment section
    experiment: 'id++', // date, audio_file_data, analysed
    experiment_info: 'id++', // , analyses_complete, participant_id, total_audio_files
    experiment_finished: 'id++', // the finished analyses; after the last task the flag "analyses_complete" is set to true
    // Pilot section
    pilot: 'id++', // date, , audio_file_data, analysed
    pilot_info: 'id++', // , analyses_complete, participant_id, total_audio_files
    pilot_finished: 'id++',
    // Playground section
    playground: 'id++', // date, analysed, audio_file_data
    playground_info: 'id++', // , analysed_audio_files_media_ids, analyses_complete, total_audio_files
    playground_finished: 'id++',
    // Snapshots
    snapshot_playground: 'id++',
    snapshot_pilot: 'id++',
    snapshot_experiment: 'id++',
    // Save audio array Buffer
    current_file: 'url,data'
});

// Add initial data
db.on("populate", () => {
    db.status.add({
        id: 0,
        isExperiment: false,
        rootFontSize: getDevicePixelRatioDependentFontSize(),
        settings: {}
    })

    db.experiment_info.add({
        id: 0,
        active_audio: {},
        analysed_audio_files_media_ids: [],
        analyses_complete: false,
        total_audio_files: 0,
    })

    db.pilot_info.add({
        id: 0,
        active_audio: {},
        analysed_audio_files_media_ids: [],
        analyses_complete: false,
        total_audio_files: 0,
    })

    db.playground_info.add({
        id: 0,
        active_audio: {},
        analysed_audio_files_media_ids: [],
        analyses_complete: false,
        total_audio_files: 0,
    })

    // Reflects the structure of Vuex's "experiment" store
    db.snapshot_playground.add({
        id: 0,
        activeAudio: {},
        combinedCues: {},
        cues: {},
        cuesOfStepOne: {},
        cuesOfStepTwo: {},
        descriptors: {},
        feedbackMessage: '',
        participantId: -1,
    })

    // Reflects the structure of Vuex's "experiment" store
    db.snapshot_pilot.add({
      id: 0,
      activeAudio: {},
      combinedCues: {},
      cues: {},
      cuesOfStepOne: {},
      cuesOfStepTwo: {},
      descriptors: {},
      feedbackMessage: '',
      participantId: -1,
      pilotId: -1
    })

    // Reflects the structure of Vuex's "experiment" store
    db.snapshot_experiment.add({
      id: 0,
      activeAudio: {},
      combinedCues: {},
      cues: {},
      cuesOfStepOne: {},
      cuesOfStepTwo: {},
      descriptors: {},
      feedbackMessage: '',
      participantId: -1,
      experimentId: -1
    })
})

db.open()

export default db