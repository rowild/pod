// https://gist.github.com/mathewbyrne/1280286
// https://stackoverflow.com/questions/1053902/how-to-convert-a-title-to-a-url-slug-in-jquery/5782563#5782563
// checkout this post, if you need German Umlauts transcibed to "ae", "oe" or "ue" or "ß" to "ss"
// https://gist.github.com/mathewbyrne/1280286#gistcomment-3753527

/**
 * Slugify any string
 * Needed for VueFormulate in PoD Application form
 *
 * @param {String} text
 * @returns
 */

export default ({ app }, inject) => {

  const slugify = text => {

    const sets = [
      { to: 'a', from: '[ÀÁÂÃÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]' },
      { to: 'ae', from: '[Ä]' },
      { to: 'c', from: '[ÇĆĈČ]' },
      { to: 'd', from: '[ÐĎĐÞ]' },
      { to: 'e', from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]' },
      { to: 'g', from: '[ĜĞĢǴ]' },
      { to: 'h', from: '[ĤḦ]' },
      { to: 'i', from: '[ÌÍÎÏĨĪĮİỈỊ]' },
      { to: 'j', from: '[Ĵ]' },
      { to: 'ij', from: '[Ĳ]' },
      { to: 'k', from: '[Ķ]' },
      { to: 'l', from: '[ĹĻĽŁ]' },
      { to: 'm', from: '[Ḿ]' },
      { to: 'n', from: '[ÑŃŅŇ]' },
      { to: 'o', from: '[ÒÓÔÕØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]' },
      { to: 'oe', from: '[ŒÖ]' },
      { to: 'p', from: '[ṕ]' },
      { to: 'r', from: '[ŔŖŘ]' },
      { to: 's', from: '[ŚŜŞŠ]' },
      { to: 'ss', from: '[ß]' },
      { to: 't', from: '[ŢŤ]' },
      { to: 'u', from: '[ÙÚÛŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]' },
      { to: 'ue', from: '[Ü]' },
      { to: 'w', from: '[ẂŴẀẄ]' },
      { to: 'x', from: '[ẍ]' },
      { to: 'y', from: '[ÝŶŸỲỴỶỸ]' },
      { to: 'z', from: '[ŹŻŽ]' },
      { to: '-', from: "[·/_,:;']" }
    ]

    sets.forEach((set) => {
      text = text.replace(new RegExp(set.from, 'gi'), set.to)
    })

    text = text.toString().toLocaleLowerCase().trim()

    text = text
      .normalize('NFD') // e.g. ö => oe
      .replace(/[\u0300-\u036F]/g, '') // remove more diacritics
      .replace(/\s+/g, '_') // Replace spaces with underscore (easier to work with in json)
      .replace(/[^\w-]+/g, '') // Remove all non-word chars
      .replace(/-+/g, '_') // Replace multiple - with single underscore (easier to work with in json)
      // .replace(/--+/g, '_') // Replace multiple - normally using at least to toomit appearance of one -, which we need anyway. But the above line converts to _
      .replace(/_+/g, '_') // Replace multiple _
      // .replace(/^-+/, '') // Trim - from start of text
      .replace(/^_+/, '') // Trim - from start of text
      // .replace(/-+$/, '') // Trim - from end of text
      .replace(/_+$/, '') // Trim - from end of text
    // .replace(/[\s_-]+/g, '-') // why is this here? maybe because of the unserscore?

    return text
  }

  // Inject $hello(msg) in Vue, context and store.
  inject('slugify', slugify)

  // For Nuxt <= 2.12, also add
  // context.$slugify = slugify
}