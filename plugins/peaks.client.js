import Vue from 'vue'
import Peaks from 'peaks.js'

Vue.use((Vue, options) => {
  Vue.prototype.$peaks = Peaks
})