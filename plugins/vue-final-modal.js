import Vue from 'vue'
import { vfmPlugin } from 'vue-final-modal/lib'

const opts = {
  key: '$vfm',
  componentName: 'VueFinalModal',
  dynamicContainerName: 'ModalsContainer'
}

Vue.use(vfmPlugin(opts))