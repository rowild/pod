// https: //github.com/nuxt/nuxt.js/issues/2554

module.exports = function (req, res, next) {
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
  next()
}