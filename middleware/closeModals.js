import { $vfm } from 'vue-final-modal';

// This does not really work. Modals are not closed. But even worse: with
// progressing through the app, the number of the router's beforeEach calls
// seems to increase drastically.

// Instead, see layouts/default.vue => watch

let counter = 0

export default function ({ store }) {
  store.app.router.beforeEach((to, from, next) => {
    console.log("Router middelware closeModals", counter++)
    $vfm.hideAll()
    next()
  })
}