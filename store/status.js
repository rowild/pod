import db from '../db/dexie'
// Export the state as a function, and the mutations and actions as objects.
// Question: Is "darkmode" really needed in Vuex? Wouldn't indexeddb suffice?

export const state = () => ({
  settings: {}, // all settings coming rom strapi (and maybe some more)
  rootFontSize: 18,
  // this is needed so that the modal for invite code does not jump up each time
  // we enter the audio file list; keep it in vuex, because we want it to be
  // set to false again as soon as the browser window is closed.
  inviteCodeAccepted: false,
  isExperiment: false,
  isTesting: false,
  // Snapshot
  snapshotPlaygroundDetected: false,
  snapshotPilotDetected: false,
  snapshotExperimentDetected: false,
  snapshotPlaygroundFromRoute: null,
  snapshotPilotFromRoute: null,
  snapshotExperimentFromRoute: null,
  // peaks Audio Player settings
  audioTrackVolume: 0.5,
  clickTrackVolume: 0.5,
})

export const getters = {
  getSettings(state) {
    return state.settings
  },
  getIsExperiment(state) {
    return state.isExperiment
  },
  getIsTesting(state) {
    return state.isTesting
  },
  getRootFontSize(state) {
    return state.rootFontSize
  },
  getInviteCodeAccepted(state) {
    return state.inviteCodeAccepted
  },
  getSnapshotPlaygroundDetected(state) {
    return state.snapshotPlaygroundDetected
  },
  getSnapshotPilotDetected(state) {
    return state.snapshotPilotDetected
  },
  getSnapshotExperimentDetected(state) {
    return state.snapshotExperimentDetected
  },
  getSnapshotPlaygroundFromRoute(state) {
    return state.snapshotPlaygroundFromRoute
  },
  getSnapshotPilotFromRoute(state) {
    return state.snapshotPilotFromRoute
  },
  getSnapshotExperimentFromRoute(state) {
    return state.snapshotExperimentFromRoute
  },
  getAudioTrackVolume(state) {
    return state.audioTrackVolume
  },
  getClickTrackVolume(state) {
    return state.clickTrackVolume
  }
}

export const mutations = {
  updateSettings(state, payload) {
    state.settings = payload
  },
  updateIsExperiment(state, bool) {
    state.isExperiment = bool
  },
  updateDevelopment(state, bool) {
    state.development = bool
  },
  updateRootFontSize(state, number) {
    state.rootFontSize = number
  },
  setInviteCodeAccepted(state, bool) {
    state.inviteCodeAccepted = bool
  },
  setSnapshotPlaygroundDetected(state, bool) {
    state.snapshotPlaygroundDetected = bool
  },
  setSnapshotPilotDetected(state, bool) {
    state.snapshotPilotDetected = bool
  },
  setSnapshotExperimentDetected(state, bool) {
    state.snapshotExperimentDetected = bool
  },
  setSnapshotPlaygroundFromRoute(state, route) {
    state.snapshotPlaygroundFromRoute = route
  },
  setSnapshotPilotFromRoute(state, route) {
    state.snapshotPilotFromRoute = route
  },
  setSnapshotExperimentFromRoute(state, route) {
    state.snapshotExperimentFromRoute = route
  },
  setAudioTrackVolume(state, vol) {
    state.audioTrackVolume = vol
  },
  setClickTrackVolume(state, vol) {
    state.clickTrackVolume = vol
  }
}

export const actions = {
  async setSettings({ commit }, payload) {
    commit('updateSettings', payload)
    await db.status.update(0, {
      settings: payload
    })
  },
  async setIsExperiment({ commit }, bool) {
    commit('updateIsExperiment', bool)
    await db.status.update(0, {
      isExperiment: bool
    })
  },
  async setRootFontSize({ commit }, size) {
    commit('updateRootFontSize', size)
    await db.status.update(0, {
      rootFontSize: size
    })
  },
}