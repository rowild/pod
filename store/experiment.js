import Vue from 'vue'
import db from '@/db/dexie'

/**
 * Cues within an experiment:
 * and experiment has two steps (2 "steps"):
 * - in the first step, the cues are set by the user
 * - in the second step the cues can be moved, deleted and new cues added
 *
 * How do we handle that info?
 * 1. step one is managed in VueX. Once this step is done, everything is saved to
 *    indexeddb, where a cue's "time" property is changed to an array: `time: ['4:10']`
 * 2. in step 2, new cues can be created, old ones deleted (only set flag"deleted")
 *    and moved around! QUESTION: can a cue be undeleted again?
 * 3. then, after step 2 is done, the NEW time value will be saved to the time
 *    property as SECOND member of the time array: `time: ['4:10','4:56']`
 *    If it was deleted, a new boolean property "deleted: true" will be added
 *    If it is a totally new entry, add a complete new cue, where the first member
 *    of the time array is null: `time: [null, '7:32']`
 *
 * So eventually dexie merges the data
 */

const consCol = 'font-weight:bold;color:deeppink'

export const state = () => ({
  activeAudioId: -1,
  activeAudio: {},
  // "Cues" is what Peaks calls "Points"; "id" must be unique (peaks creates it automatically if non given)
  cues: [],
  cuesOfStepOne: [],
  cuesOfStepTwo: [], // used in "saveTask()", where final array is build
  combinedCues: [], // used in "saveTask()", where final array is build
  descriptors: [],
  // Only in Pilot: after finishing the experiment, participants can send feedback
  feedbackMessage: ''
})

export const getters = {
  getAudioFileId(state) {
    return state.audioFileId
  },
  getActiveAudio(state) {
    return state.activeAudio
  },
  getCues(state) {
    return state.cues
  },
  getCuesOfStepOne(state) {
    return state.cuesOfStepOne
  },
  getDescriptors(state) {
    return state.descriptors
  },
  getFeedbackMessage(state) {
    return state.feedbackMessage
  }
}

export const mutations = {
  // add new peaks point (= cue)
  addCue(state, payload) {
    console.group('%cStore/experiment: addCue invoked', consCol)
    console.log('payload =', payload);
    state.cues.push(payload)
    console.groupEnd()
  },

  // Find and merge an existing cue with newly provided data
  cueUpdate(state, payload) {
    console.group('%cStore/experiment: cueUpdate invoked', consCol)
    console.log('payload =', payload, '\n');

    const index = state.cues.findIndex(cue => {
      // console.log('cue =', cue);
      // console.log('cue.id =', cue.id);
      return cue.id === payload.id
    })

    console.log('index =', index);

    Object.entries(payload).forEach(([key, value]) => {
      if (value instanceof Array) {
        console.log('   value instanceof Array: key =', key, ', value =', value);
        Vue.set(state.cues[index], key, [...value])
      }
      else {
        console.log('   value is NOT instanceof Array: key =', key, ', value =', value);
        Vue.set(state.cues[index], key, value)
      }
    })

    console.groupEnd()
  },

  // load cues when restoreTask = true
  restoreTaskFromSnapshot(state, payload) {
    console.log(' ');
    console.log('------');
    console.group('%cStore/experiment: restoreTaskFromSnapshot invoked', consCol)
    console.log('payload =', payload);

    state.activeAudio = JSON.parse(JSON.stringify(payload[0].activeAudio))
    state.activeAudioId = JSON.parse(JSON.stringify(payload[0].activeAudioId))

    // state.cues = JSON.parse(JSON.stringify(payload[0].cues))
    const cues = payload[0].cues
    if(cues) {
      // console.log('\n\n\ncues =', cues);
      state.cues = cues.map(item => {
        // console.log('item =', item);
        return {...item}
      })
      // console.log('\n\n\n');
    }

    // state.cuesOfStepOne = JSON.parse(JSON.stringify(payload[0].cuesOfStepOne))
    const cuesStep1 = payload[0].cuesOfStepOne
    if (cuesStep1) {
      // console.log('\n\n\ncuesStep1 =', cuesStep1);
      state.cuesOfStepOne = cuesStep1.map(item => {
        // console.log('item =', item);
        return {...item}
      })
      // console.log('\n\n\n');
    }

    state.descriptors = JSON.parse(JSON.stringify(payload[0].descriptors))

    console.groupEnd()
    console.log('------');
    console.log(' ');
  },

  /**
   * Reset cues to empty array
   * Called after an anylsis is done and saved
   */
  resetCues(state) {
    console.group('%cStore/experiment: resetCues invoked', consCol)
    console.log('state =', state);
    state.cues = []
    state.cuesOfStepOne = []
    state.cuesOfStepTwo = []
    state.combinedCues = []
    console.groupEnd()
  },

  saveCuesOfStepOne(state, payload) {
    console.group('%cStore/experiment: saveCuesOfStepOne invoked', consCol)
    console.log('payload =', payload);
    // use spread to create NEW objects (otherwise they are reactive and
    // get updated along with updates to the original points)
    // or: "JSON.parse(JSON.stringify(..."
    // src: https://stackoverflow.com/questions/54801956/vue-changes-array-in-data-after-making-a-copy-of-it
    state.cuesOfStepOne = payload.map(item => ({...item}))
    console.groupEnd()
  },

  feedbackMessage(state, payload) {
    console.log('%cStore/experiment: feedbackMessage invoked', consCol)
    console.log('payload =', payload);
    state.feedbackMessage = payload
    console.groupEnd()
  },

  setActiveAudio(state, payload) {
    state.activeAudio = payload
  },

  setActiveAudioId(state, number) {
    state.activeAudioId = number
  }
}

export const actions = {
  /**
   * Save the current task to dexie
   *
   * @param {Object} context Vuex's state and commit methods from context
   * @param {Object} payload The data to be saved
   */
  async setActiveAudio({ state, commit }, payload) {
    console.log(' ');
    console.log('----------------------');
    console.groupCollapsed('%cStore/experiment: setActiveAudio invoked: payload =', consCol, payload);
    commit('setActiveAudio', payload.file)
    commit('setActiveAudioId', payload.file.id)

    let updateActiveAudioInDb = 0

    if (!payload.isExperiment) {
      console.log('update active audio file in PLAYGROUND');
      updateActiveAudioInDb = await db.playground_info.update(0, {
        active_audio_id: payload.file.id,
        active_audio: payload.file
      })
    }
    else if (payload.isExperiment && payload.phase === 'PILOT') {
      console.log('update active audio file in PILOT');
      updateActiveAudioInDb = await db.pilot_info.update(0, {
        active_audio_id: payload.file.id,
        active_audio: payload.file
      })
    }
    else if (payload.isExperiment && payload.phase === 'EXPERIMENT') {
      console.log('update active audio file in EXPERIMENT');
      updateActiveAudioInDb = await db.experiment_info.update(0, {
        active_audio_id: payload.file.id,
        active_audio: payload.file
      })
    }

    console.log('return Value of updateActiveAudioInDb =', updateActiveAudioInDb);
    console.groupEnd()
    console.log('----------------------');
    console.log(' ');

    // Return sth to the caller, so the change to the Waveform page can happen
    return updateActiveAudioInDb
  },

  /**
   * All the things that need to be done when saving an analysis
   *
   * 1. saving to indexedDB [playground|pilot|experiment]_info
   * 2. saving to strapi
   * 3. deleting cues from VueX
   * ...and more... ?
   *
   * In Nuxt, use "context" if calls to other getters, actions etc need to be done
   *
   * @param {Object} context   Nuxt context object
   * @param {Object} payload   Delivers fileData and phase (f the project)
   *
   * @returns {Object}
   */
  async saveTask(context, payload) {
    console.log(' ');
    console.log('-----------------------');
    console.groupCollapsed('%cStore/experiment: saveTask', consCol)
    console.log('payload =', payload);
    console.log('payload.phase =', payload.phase);
    console.log('payload.isExperiment =', payload.isExperiment);
    console.log('payload.appSection =', payload.appSection);

    // 1. set file to analysed and provide the updated fileData to the next process
    const cuesOfStepTwo = context.state.cues // current cues; here they should be saved to the store's cueOfStepTwo state
    const cuesOfStepOne = context.state.cuesOfStepOne
    const combinedCues = Object.assign({}, cuesOfStepOne, cuesOfStepTwo)

    let resultToSave = {}

    // RegData is required, which is not necessarily the case, when we enter the
    // Playground for the first (and even subsequent) time
    if (payload.regData && Object.keys(payload.regData).length > 0) {
      // Construct filename
      const f1 = `partid-${payload.regData.id}_`
      const f3 = `taskid-${payload.fileData.id}`
      let f2 = `pilotid-${payload.regData.pilot_id}_`
      if (payload.regData.experiment_id !== undefined) {
        f2 = `groupid-${payload.regData.experiment_id}_`
      }

      // These fields must match strapi's "results" db fields (there are relations!)
      resultToSave = {
        pilot_experiment_id: -1,
        pilot_participant_id: -1, // always (person id is unique)
        experiment_participant_id: -1, // always (person id is unique)
        experiment_group_id: -1,
        filename: f1 + f2 + f3,
        task_id: payload.fileData.id, // the file id = task_id
        expert: payload.regData.jsondata.musical_expertise.expert,
        // "restults" is a jsondata field in the "results" database table
        results: {
          fileId: payload.fileData.id, // = task_id
          fileData: payload.fileData,
          cuesOfStepOne,
          cuesOfStepTwo,
          combinedCues,
          participant_data: {
            id: payload.regData.id,
            birthDay: payload.regData.jsondata.person.date_of_birth,
            gender: payload.regData.jsondata.person.gender,
            expert: payload.regData.jsondata.musical_expertise.expert,
            listeningExperience: payload.regData.jsondata.musical_expertise.listening_experience,
            trainingFocus: payload.regData.jsondata.musical_expertise.training_focus,
            trainingLevel: payload.regData.jsondata.musical_expertise.training_level,
          }
        }
      }

      if (payload.phase === 'PILOT') {
        resultToSave.pilot_participant_id = payload.regData.id
        resultToSave.pilot_experiment_id = payload.regData.pilot_id
      }
      else if (payload.phase === 'EXPERIMENT') {
        resultToSave.experiment_participant_id = payload.regData.id
        resultToSave.experiment_group_id = payload.regData.experiment_id // only in experimentphase
      }
    }
    else {
      resultToSave = {
        testDataFromPlayground: true,
        pilot_experiment_id: null,
        pilot_participant_id: null, // always (person id is unique)
        experiment_group_id: null, // only in experimentphase
        experiment_participant_id: null, // always (person id is unique)
        filename: 'playground-test',
        task_id: 0, // the file id = task_id
        expert: false,
        participant_data: {}
      }
    }

    let appSection = 'playground'
    if (payload.isExperiment && (payload.phase === 'EXPERIMENT')) {
      appSection = 'experiment'
    }
    else if (payload.isExperiment && (payload.phase === 'PILOT')) {
      appSection = 'pilot'
    }

    /* --------------- Save to indexeddB & strapi --------------- */

    try {
      await context.dispatch('saveTaskToIndexedDB', { appSection, resultToSave, payload })

      if (appSection === 'playground') {
        console.log('appSection = ', appSection)
        console.log('> Store/Experiment: PLAYGROUND: we do not save to a DB, when we are in playground.');
      }
      else if (appSection === 'pilot') {
        console.log('appSection =', appSection)
        // console.log('   resultToSave = ', resultToSave)
        // console.log('   payload.isExperiment =', payload.isExperiment)
        // console.log('   payload.phase =', payload.phase)
        // console.log('   payload =', payload)
        context.dispatch('saveToPilot', resultToSave )
      }
      else if (appSection === 'experiment') {
        console.log('appSection = ', appSection)
        context.dispatch('saveToExperiment', resultToSave )
      }

      // Delete cues (cuesOfFirstStep, cuesOfSecondStep, combinedCues) from Vuex
      context.commit('resetCues')
    }
    catch (err) {
      console.error('An error saving to indexedDB happened: error =', err)
    }

    console.groupEnd()
    console.log('----------------------------------------');
    console.log(' ');

    return true
  },

  /**
   * Save the result of a task to indexeddb and to strapi, including registraton
   * data.
   *
   * @param {Object} context A VueX object
   * @param {Object} data    The data array to be saved
   */
  async saveTaskToIndexedDB(context, data) {
    console.groupCollapsed('%cStore/experiment: saveTaskToIndexedDB', consCol);
    console.log('data =', data);
    console.log('data.appSection =', data.appSection);
    console.log('data.resultToSave =', data.resultToSave);

    /**
     * Save the result of the analysed task to a seperate table (as Backup;
     * maybe save to strapi from here)
     */
    const savedToFinishedTbl = await db.table(data.appSection + '_finished').put(data.resultToSave, {
      id: data.payload.fileData.fileId
    })
    console.log('savedToFinishedTbl =', savedToFinishedTbl);

    // Set currently analysed audio file's "analysed" property to "true"
    const setAnalysedAttributeToTrue = await db.table(data.appSection).update(data.payload.fileData.id, { analysed: true })
    console.log('setAnalysedAttributeToTrue =', setAnalysedAttributeToTrue);

    /**
     * Save "mediaId" of analysed file to [playground|pilot|experiment]Info.
     *
     * This is needed in case the "Refresh from server" function is called which
     * overrides all "analysed" properties and sets them to false again. Therefore
     * a follow-up step compares the mediaIds with "playground-info[0].analysed_audio_files_media_ids"
     * and resets the audio file's "analysed" property to its proper value again;
     * Done in Playground.vue => update())
     *
     * To increment "analysed_audio_files_media_ids" array, first get the info.
     * Then extract the analysed_audio_files_media_ids array and push the latest
     * analysed file's mediaId onto it
     */
    const infoTbl = await db.table(data.appSection + '_info').toArray()
    const analysedAudioFilesMediaIds = infoTbl[0].analysed_audio_files_media_ids
    analysedAudioFilesMediaIds.push(data.payload.fileData.mediaId)
    const updatedMediaIds = await db.table(data.appSection + '_info').update(0, {
      analysed_audio_files_media_ids: analysedAudioFilesMediaIds
    })
    console.log('updatedMediaIds =', updatedMediaIds);

    // Check whether all analyses have been completed or not
    if (analysedAudioFilesMediaIds.length === infoTbl[0].total_audio_files) {
      const updateAnalysesComplete = await db.table(data.appSection + '_info').update(0, {
        analyses_complete: true
      })
      console.log('updateAnalysesComplete =', updateAnalysesComplete);
      console.log('All analyses completed. Please go back to the task selection list.');
    } else {
      console.log('Not all the analyses are complete yet. There are further experiments to do, so go back to the selection list...');
    }

    console.groupEnd()
  },

  saveToPilot(context, payload) {
    console.log(' ');
    console.groupCollapsed('----------------------------------------');
    console.log('%cStore/experiment: saveToPilot', consCol);
    console.log('payload =', payload); // should be resultToSave

    const saveToStrapiResult = this.$axios.post('/results', payload)
      .then(response => {
        console.log('saveToStrapiResult response =', response);
        return response
      })
      .catch(error => {
        console.log(' ');
        console.error('%c:::::::::','color:crimson');
        console.error('error', error);
        console.error('error.response', error.response);
        console.error('Task must be unique; it seems a task with this ID already exists on server side in the Strapi backend.')
        console.error('%c:::::::::','color:crimson');
        console.log(' ');
        return error.response
      })
      .finally(() => {
        console.groupEnd()
        console.log('----------------------------------------');
        console.log(' ');
      })
  },

  saveToExperiment(context, payload) {
    console.group('%cStore/Experiment: saveToExperiment to be implemented yet.', consCol);
    console.groupEnd();

    return true
  },

  /**
   * As soon as step one has finished, we copy the current cues to a new place,
   * which eventually will become the final data. In the course of copying the
   * cues, we modify the time to an array, which eventuall will hold 2 values.
   * The 2nd value will be the time of step two.
   * There will be other elements merged to the final data object (like hidden,
   * annotations, ...), but that's for later.
   *
   * Called from Wavefrom:proceedToNextExperimentStep()
   * Saving to cuesOfStepTwo is invoked in saveTask()
   *
   * @param {ob} context
   */
  copyCuesOfStepOne(context) {
    console.groupCollapsed('%cStore/Experiment: copyCuesOfStepOne.', consCol);
    const currentCues = context.getters.getCues

    const cuesOfStepOne = currentCues.map(item => {
      // item.timeValues = []
      // item.timeValues.push(item.time)
      return item
    })

    context.commit('saveCuesOfStepOne', cuesOfStepOne)

    console.groupEnd();
  },

  /**
   * Save Snapshot to dexie
   */
  async saveSnapshotToDb(context) {
    console.groupCollapsed('%cStore/Experiment: saveSnapshotToDB', consCol);

    // Save the result of the analysed task to a seperate table (as Backup; maybe save to strapi from here)
    const savedToCurrentTaskdTbl = await db.table('current_task').put(data.resultToSave, {
      id: data.payload.fileData.fileId
    })
    console.log('savedToCurrentTaskdTbl =', savedToCurrentTaskdTbl);

    console.groupEnd()
  },
}