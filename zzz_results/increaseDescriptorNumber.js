// Increase descriptor numbers by 1
// Since numbering started at 0, but 1 is dsired

'use strict';
const fs = require('fs');
const axios = require('axios')

/**
 * As of 2022-02-15:
 * Script has increased all descriptors (of tasks id 101-116) by 1
 * don't execute anymore this script anymore on those tasls, since that would
 * alter the data badly and make them unusable
 *
 * TODO: implement a check like "changed: true"
 */
const resultId = null
const url = `https://strapi-pod.herokuapp.com/results/${resultId}`

// https://stackoverflow.com/questions/32796230/with-javascript-how-would-i-increment-numbers-in-an-array-using-a-for-loop
function addOne(arr) {
  return Array.from(arr, x => (++x).toString());
}

const getResult = async() => {
  try {
    const getResults = await axios.get(url)
    const data = JSON.stringify(getResults.data, null, 2);
    const filename = `result-${resultId}-orig.json`

    if(fs.existsSync('./zzz_results/' + filename)) {
      console.log('file already exists: ', filename);
    }

    fs.writeFileSync('./zzz_results/' + filename, data);
  }
  catch(error) {
    console.error('Error loading result from strapi: error =', error)
  }
}

const fixResult = async() => {
  const result = await getResult()

  const rawdata = fs.readFileSync(`./zzz_results/result-${resultId}-orig.json`);
  const results = JSON.parse(rawdata);

  const combinedCues = results.results.combinedCues
  const lenComb = Object.keys(combinedCues).length
  for(let i = 0; i < lenComb; i++) {
    const descr = combinedCues[i].descriptors
    if(descr.length) {
      combinedCues[i].descriptors = addOne(descr)
    }
  }

  const cuesOfStepTwo = results.results.cuesOfStepTwo
  const lenStepTwo = Object.keys(cuesOfStepTwo).length
  for(let i = 0; i < lenStepTwo; i++) {
    const descr = cuesOfStepTwo[i].descriptors
    if(descr.length) {
      cuesOfStepTwo[i].descriptors = addOne(descr)
    }
  }

  // It seems some fields cannot be updated (probably because editing of some
  // fields is not allowed in strapi BE?) So delete everything, that does not
  // belong to the JSON field
  delete results.id
  delete results.participant_id
  delete results.result
  delete results.experiment_group
  delete results.published_at
  delete results.created_at
  delete results.updated_at
  delete results.experiment_participant_id
  delete results.pilot_participant_id
  delete results.experiment_group_id
  delete results.pilot_experiment_id
  delete results.task_id
  delete results.expert
  delete results.filename

  // console.log('result =', results);

  // write to resultsNew.js
  const data = JSON.stringify(results, null, 2);
  fs.writeFileSync(`./zzz_results/result-${resultId}-new.json`, data);

  // Put new results to strapi
  try {
    const res = await putResults (results)
    console.log('res =', res);
  }
  catch(err) {
    throw new Error(err, err.message)
  }
}

async function putResults (body) {
 try {
  const response = await axios.put(url, body);
  console.log('response.data =', response.data);
  return response.data;
 }
 catch (err) {
  throw new Error(err, err.message);
 }
}


fixResult()